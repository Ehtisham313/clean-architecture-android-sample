package com.presentation.factory

import com.domain.model.Vehicle
import com.presentation.model.VehicleView

object VehiclesFactory {

    fun makeVehiclesView(): VehicleView {
        return VehicleView(DataFactory.randomInt(),DataFactory.randomDouble(),
                           DataFactory.randomDouble(),DataFactory.randomFleetType(),DataFactory.randomDouble())
    }

    fun makeVehicles(): Vehicle {
        return Vehicle(DataFactory.randomInt(),DataFactory.randomDouble(),
            DataFactory.randomDouble(),DataFactory.randomFleetType(),DataFactory.randomDouble())
    }

    fun makeVehiclesViewList(count: Int): List<VehicleView> {
        val vehicles = mutableListOf<VehicleView>()
        repeat(count) {
            vehicles.add(makeVehiclesView())
        }
        return vehicles
    }

    fun makeVehiclesList(count: Int): List<Vehicle> {
        val vehicles = mutableListOf<Vehicle>()
        repeat(count) {
            vehicles.add(makeVehicles())
        }
        return vehicles
    }
}