package com.presentation.mapper

import com.presentation.factory.VehiclesFactory
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class VehiclesViewMapperTest {

    private val mapper = VehicleViewMapper()

    @Test
    fun mapToViewMapsData() {
        val vehicle = VehiclesFactory.makeVehicles()
        val vehicleView = mapper.mapToView(vehicle)

        assertEquals(vehicle.id, vehicleView.id)
        assertEquals(vehicle.latitude, vehicleView.latitude)
        assertEquals(vehicle.longitude, vehicleView.longitude)
        assertEquals(vehicle.fleetType, vehicleView.fleetType)
        assertEquals(vehicle.heading, vehicleView.heading)

    }

}