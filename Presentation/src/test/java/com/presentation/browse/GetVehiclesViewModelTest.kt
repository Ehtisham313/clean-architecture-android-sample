package com.presentation.browse

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.domain.interactor.vehicle.GetVehicles
import com.domain.interactor.vehicle.GetVehiclesInBound
import com.domain.model.Vehicle
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.presentation.GetVehiclesViewModel
import com.presentation.factory.DataFactory
import com.presentation.factory.VehiclesFactory
import com.presentation.mapper.VehicleViewMapper
import com.presentation.model.VehicleView
import com.presentation.state.ResourceState
import io.reactivex.observers.DisposableObserver
import io.reactivex.subscribers.DisposableSubscriber
import junit.framework.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Captor

@RunWith(JUnit4::class)
class GetVehiclesViewModelTest {

    @get:Rule var instantTaskExecutorRule=InstantTaskExecutorRule()
    var getVehicles=mock<GetVehicles>()
    var getVehiclesInBound=mock<GetVehiclesInBound>()
    var vehicleViewMapper=mock<VehicleViewMapper>()

    var mVehicleViewModel=GetVehiclesViewModel(getVehicles,getVehiclesInBound,vehicleViewMapper)

    @Captor
    val captor= argumentCaptor<DisposableSubscriber<List<Vehicle>>>()

    @Test
    fun fetchVehicleExecuteUseCase(){
        mVehicleViewModel.fetchVehicles()
        verify(getVehicles,times(1)).execute(any(),eq(null))
    }

    @Test
    fun fetchVehiclesInBoundExecuteUseCase(){
        val constantDouble=DataFactory.randomDouble()
        mVehicleViewModel.fetchVehcilesInMapBound(
            constantDouble,constantDouble,
            constantDouble,constantDouble)
        verify(getVehiclesInBound,times(1)).execute(any(),
            eq(GetVehiclesInBound.Params.forProject(
                constantDouble,constantDouble,
                constantDouble,constantDouble)))

    }


    @Test
    fun fetchVehiclesReturnSuccess(){
        val vehicles=VehiclesFactory.makeVehiclesList(2)
        val vehicleView=VehiclesFactory.makeVehiclesViewList(2)
        stubVehiclesMapperMapToView(vehicleView[0],vehicles[0])
        stubVehiclesMapperMapToView(vehicleView[1],vehicles[1])
        mVehicleViewModel.fetchVehicles()
        verify(getVehicles).execute(captor.capture(),eq(null))
        captor.firstValue.onNext(vehicles)
        assertEquals(ResourceState.SUCCESS,mVehicleViewModel.getVehicles().value?.status)
    }

    @Test
    fun fetchVehiclesInBoundReturnData(){
        val vehicles=VehiclesFactory.makeVehiclesList(2)
        val vehicleView=VehiclesFactory.makeVehiclesViewList(2)
        stubVehiclesMapperMapToView(vehicleView[0],vehicles[0])
        stubVehiclesMapperMapToView(vehicleView[1],vehicles[1])

        val constantDouble=DataFactory.randomDouble()
        mVehicleViewModel.fetchVehcilesInMapBound(
            constantDouble,constantDouble,
            constantDouble,constantDouble)

        verify(getVehiclesInBound).execute(captor.capture(),
            eq(GetVehiclesInBound.Params.forProject
                (constantDouble,constantDouble,
                constantDouble,constantDouble)))

        captor.firstValue.onNext(vehicles)
        assertEquals(ResourceState.SUCCESS,mVehicleViewModel.getVehicles().value?.status)

    }


    @Test
    fun fetchVehiclesReturnData(){
        val vehicles=VehiclesFactory.makeVehiclesList(2)
        val vehicleView=VehiclesFactory.makeVehiclesViewList(2)
        stubVehiclesMapperMapToView(vehicleView[0],vehicles[0])
        stubVehiclesMapperMapToView(vehicleView[1],vehicles[1])
        mVehicleViewModel.fetchVehicles()
        verify(getVehicles).execute(captor.capture(),eq(null))
        captor.firstValue.onNext(vehicles)
        assertEquals(vehicleView,mVehicleViewModel.getVehicles().value?.data)
    }


    @Test
    fun fetchVehiclesReturnError(){
        mVehicleViewModel.fetchVehicles()
        verify(getVehicles).execute(captor.capture(),eq(null))
        captor.firstValue.onError(RuntimeException())
        assertEquals(ResourceState.ERROR,mVehicleViewModel.getVehicles().value?.status)
    }

    @Test
    fun fetchVehiclesInBoundReturnError(){

        val constantDouble=DataFactory.randomDouble()
        mVehicleViewModel.fetchVehcilesInMapBound(
            constantDouble,constantDouble,
            constantDouble,constantDouble)

        verify(getVehiclesInBound).execute(captor.capture(),
            eq(GetVehiclesInBound.Params.forProject
                (constantDouble,constantDouble,
                constantDouble,constantDouble)))
        captor.firstValue.onError(java.lang.RuntimeException())
        assertEquals(ResourceState.ERROR,mVehicleViewModel.getVehicles().value?.status)

    }


    @Test
    fun fetchVehiclesReturnErrorMessage(){
        val errorMessage = DataFactory.randomString()
        mVehicleViewModel.fetchVehicles()
        verify(getVehicles).execute(captor.capture(),eq(null))
        captor.firstValue.onError(RuntimeException(errorMessage))
        assertEquals(errorMessage,mVehicleViewModel.getVehicles().value?.message)
    }


    @Test
    fun fetchVehiclesInBoundReturnMessage(){

        val message=DataFactory.randomString()
        val constantDouble=DataFactory.randomDouble()
        mVehicleViewModel.fetchVehcilesInMapBound(
            constantDouble,constantDouble,
            constantDouble,constantDouble)

        verify(getVehiclesInBound).execute(captor.capture(),
            eq(GetVehiclesInBound.Params.forProject
                (constantDouble,constantDouble,
                constantDouble,constantDouble)))
        captor.firstValue.onError(java.lang.RuntimeException(message))
        assertEquals(message,mVehicleViewModel.getVehicles().value?.message)
    }

    private fun stubVehiclesMapperMapToView(vehiclesView: VehicleView,
                                            vehicles: Vehicle
    ) {
        whenever(vehicleViewMapper.mapToView(vehicles))
            .thenReturn(vehiclesView)
    }


}