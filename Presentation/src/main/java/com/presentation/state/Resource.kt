package com.presentation.state

/**
* @author Ehtisham haq
 * Our class to provide view state to our presentation module
 * @param status Status Current status
 * @see ResourceState
 * @param data of Generic Type since we can have state error  it could be null
 * @param message if it is success or loading we can have message null, especially when its loading
**/
class Resource<out T> constructor(val status: ResourceState,
                                  val data: T?,
                                  val message: String?) {

    fun <T> success(data: T): Resource<T> {
        return Resource(ResourceState.SUCCESS, data, null)
    }

    fun <T> error(message: String?): Resource<T> {
        return Resource(ResourceState.ERROR, null, message)
    }

    fun <T> loading(): Resource<T> {
        return Resource(ResourceState.LOADING, null, null)
    }

}