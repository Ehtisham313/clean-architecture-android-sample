package com.presentation.state

/**
 * @author Ehtisham haq
 * Enum to define the State
 **/
enum class ResourceState {
    LOADING, SUCCESS, ERROR
}