package com.presentation


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.domain.interactor.signin.SignIn
import com.domain.interactor.signup.SignUp
import com.domain.interactor.vehicle.GetVehicles
import com.domain.interactor.vehicle.GetVehiclesInBound
import com.domain.model.Vehicle
import com.presentation.mapper.VehicleViewMapper
import com.presentation.model.VehicleView
import com.presentation.state.Resource
import com.presentation.state.ResourceState
import io.reactivex.observers.DisposableObserver
import io.reactivex.subscribers.DisposableSubscriber
import javax.inject.Inject

open class SignUpViewModel @Inject internal constructor(
                                        private val signUP: SignUp?,
                                        private val signIN: SignIn?)
                                            : ViewModel() {



    override fun onCleared() {
        signUP?.dispose()
        signIN?.dispose()
        super.onCleared()
    }

    fun signUP() {
        signUP?.execute(
            SignUpSubscriber(), SignUp.Body.forProject(
                firstName = "Nauman",
                lastName = "Yaqub",
                email = "nomi.haanny@gmaail.com",
                password = "12345678",
                confirmPassword = "1234567",
                phone = "0523313643"
            )
        )
    }


    fun signIn() {
        signIN?.execute(SignInSubscriber(), SignIn.Body.forProject(
            email =  "nomi.haanny@gmaaail.com",
            password = "12345678"
        ))
    }



    inner class SignInSubscriber : DisposableSubscriber<com.domain.model.signin.SignIn>() {

        override fun onComplete() {}

        override fun onError(e: Throwable) {
        }

        override fun onNext(t: com.domain.model.signin.SignIn?) {
        }

    }


    inner class SignUpSubscriber : DisposableSubscriber<com.domain.model.signup.SignUp>() {

        override fun onComplete() {}

        override fun onError(e: Throwable) {
        }

        override fun onNext(t: com.domain.model.signup.SignUp?) {
        }

    }

}