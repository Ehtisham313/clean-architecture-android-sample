package com.presentation.mapper

import com.domain.model.Vehicle
import com.presentation.model.VehicleView
import javax.inject.Inject

open class VehicleViewMapper @Inject constructor() : Mapper<VehicleView, Vehicle> {

    override fun mapToView(type: Vehicle): VehicleView {
        return VehicleView(type.id,
                           type.latitude,type.longitude,
                           type.fleetType,type.heading )
    }
}