package com.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.domain.interactor.signup.SignUp
import com.domain.interactor.vehicle.GetVehicles
import com.domain.interactor.vehicle.GetVehiclesInBound
import com.domain.model.Vehicle
import com.presentation.mapper.VehicleViewMapper
import com.presentation.model.VehicleView
import com.presentation.state.Resource
import com.presentation.state.ResourceState
import io.reactivex.observers.DisposableObserver
import io.reactivex.subscribers.DisposableSubscriber
import javax.inject.Inject

open class GetVehiclesViewModel @Inject internal constructor(
    private val getVehicles: GetVehicles?,
    private val getVehiclesInBound: GetVehiclesInBound?,
    private val signUP: SignUp?,
    private val mapper: VehicleViewMapper
) : ViewModel() {


    private val liveData: MutableLiveData<Resource<List<VehicleView>>> = MutableLiveData()

    override fun onCleared() {
        getVehicles?.dispose()
        getVehiclesInBound?.dispose()
        super.onCleared()
    }

    fun getVehicles(): LiveData<Resource<List<VehicleView>>> {
        return liveData
    }

    fun fetchVehicles() {
        liveData.postValue(Resource(ResourceState.LOADING, null, null))
        getVehicles?.execute(VehiclesSubscriber())

    }

    fun signUP() {
        signUP?.execute(
            SignUpSubscriber(), SignUp.Body.forProject(
                firstName = "Nauman",
                lastName = "Yaqub",
                email = "nomi.haanny@gmaail.com",
                password = "12345678",
                confirmPassword = "1234567",
                phone = "0523313643"
            )
        )
    }


    fun fetchVehcilesInMapBound(lat1: Double, lng1: Double, lat2: Double, lng2: Double) {
        getVehiclesInBound?.execute(
            VehiclesSubscriber(),
            GetVehiclesInBound.Params.forProject(lat1, lng1, lat2, lng2)
        )
    }


    inner class SignUpSubscriber : DisposableSubscriber<com.domain.model.signup.SignUp>() {

        override fun onComplete() {}

        override fun onError(e: Throwable) {
            liveData.postValue(Resource(ResourceState.ERROR, null, e.localizedMessage))
        }

        override fun onNext(t: com.domain.model.signup.SignUp?) {
        }

    }


    inner class VehiclesSubscriber : DisposableSubscriber<List<Vehicle>>() {
        override fun onNext(t: List<Vehicle>) {
            liveData.postValue(Resource(ResourceState.SUCCESS,
                t.map { mapper.mapToView(it) }, null
            )
            )
        }

        override fun onComplete() {}

        override fun onError(e: Throwable) {
            liveData.postValue(Resource(ResourceState.ERROR, null, e.localizedMessage))
        }

    }

}