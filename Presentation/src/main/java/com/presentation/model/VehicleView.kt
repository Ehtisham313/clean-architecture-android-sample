package com.presentation.model

class VehicleView(val id:Int,
                  val latitude:Double,val longitude:Double,
                  val fleetType:String,val heading:Double)