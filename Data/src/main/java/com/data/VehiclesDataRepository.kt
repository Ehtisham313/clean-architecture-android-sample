package com.data

import com.data.mapper.VehicleMapper
import com.data.model.VehicleEntity
import com.data.repository.VehicleCache
import com.data.store.VehicleDataStoreFactory
import com.domain.model.Vehicle
import com.domain.model.signin.SignIn
import com.domain.model.signup.SignUp
import com.domain.repository.POIRepository
import io.reactivex.functions.BiFunction
import javax.inject.Inject
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

class VehiclesDataRepository @Inject constructor(
    val mVehicleMapper: VehicleMapper,
    val mVehicleCache: VehicleCache,
    val mVehicleDataStoreFactory: VehicleDataStoreFactory
) : POIRepository {

    override fun getVehicles(): Flowable<List<Vehicle>> {
        return Flowable.zip(mVehicleCache.areVehiclesCached().toFlowable(),
            mVehicleCache.isVehiclesCacheExpired().toFlowable(),
            BiFunction<Boolean, Boolean, Pair<Boolean, Boolean>> { areCached, isExpired ->
                Pair(areCached, isExpired)
            })
            .flatMap {
                mVehicleDataStoreFactory.getDataStore(it.first, it.second).getVehicles()
                    .distinctUntilChanged()
            }.flatMapSingle { vehicles ->
                mVehicleDataStoreFactory.getCacheDataStore()
                    .saveVehicles(vehicles)
                    .andThen(Single.just(vehicles))
            }.map { vehicleEntity ->
                vehicleEntity.map { mVehicleMapper.mapFromEntity(it) }
            }

    }


    /*
     * The method will be used to get vehicles in Map Bound
     * We also have query on local DB to get data but it will return uas null or ambigous values incase bounds are out of hamesberg
     * So we will query server
     */
    override fun getVehiclesInBound(lat1: Double, lng1: Double, lat2: Double, lng2: Double): Flowable<List<Vehicle>> {
        return mVehicleDataStoreFactory
            .getRemoteDataStore()
            .getVehiclesInBound(lat1, lng1, lat2, lng2).map {
                it.map {
                    mVehicleMapper.mapFromEntity(it)
                }
            }
    }

    override fun signUP(
        password: String,
        phone: String,
        lastName: String,
        firstName: String,
        email: String,
        confirmPassword: String
    ): Flowable<SignUp> {
        return mVehicleDataStoreFactory.getRemoteDataStore().signUP( password,
            phone,
            lastName,
            firstName,
            email,
            confirmPassword)
    }

    override fun signIn(email: String, password: String): Flowable<SignIn> {
        return  mVehicleDataStoreFactory.getRemoteDataStore().signIn(email,password)
    }


    override fun getSelectedVehicle(): Observable<Vehicle> {
        return mVehicleDataStoreFactory.getCacheDataStore()
            .getSelectedVehicle().toObservable()
            .map { vehicleEntity ->
                mVehicleMapper.mapFromEntity(vehicleEntity)
            }
    }

    override fun selectVehicle(vehicleID: Int): Completable {
        return mVehicleDataStoreFactory.getCacheDataStore().selectVehicle(vehicleID)
    }

    override fun unSelectVehicle(vehicleID: Int): Completable {
        return mVehicleDataStoreFactory.getCacheDataStore().unSelectVehicle(vehicleID)
    }

}