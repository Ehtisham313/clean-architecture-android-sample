package com.data.repository

import com.data.model.VehicleEntity
import com.domain.model.signin.SignIn
import com.domain.model.signup.SignUp
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * @author Ehtisham haq
 * Vehicle Data store is layer between Cache and Remote Layer
 **/
interface VehicleDataStore{
    fun clearVehicles(): Completable

    fun saveVehicles(vehicles: List<VehicleEntity>): Completable

    fun getVehicles(): Flowable<List<VehicleEntity>>

    fun getVehiclesInBound(lat1: Double, lng1: Double, lat2: Double, lng2: Double):Flowable<List<VehicleEntity>>

    fun getSelectedVehicle(): Flowable<VehicleEntity>

    fun selectVehicle(vehicleId: Int): Completable

    fun unSelectVehicle(vehicleId: Int): Completable


    fun signUP(  password: String,
                 phone: String,
                 lastName: String,
                 firstName: String,
                 email: String,
                 confirmPassword: String):Flowable<SignUp>

    fun signIn(email:String, password: String):Flowable<SignIn>

}