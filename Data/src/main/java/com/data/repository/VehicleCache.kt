package com.data.repository

import com.data.model.VehicleEntity
import com.domain.model.Vehicle
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single


/**
 * @author Ehtisham haq
 * Interface for Vehicle Cache
 **/
interface VehicleCache{

    fun clearVehicles(): Completable

    fun saveVehicles(vehicles: List<VehicleEntity>): Completable

    fun getVehicles(): Flowable<List<VehicleEntity>>

    fun getSelectedVehicle(): Flowable<VehicleEntity>

    fun selectVehicle(vehicleId: Int): Completable

    fun unSelectVehicle(vehicleId: Int): Completable

    fun areVehiclesCached(): Single<Boolean>

    fun setLastCacheTime(lastCache: Long): Completable

    fun isVehiclesCacheExpired(): Single<Boolean>

    fun getVehiclesInBound(lat1:Double,lng1:Double,lat2:Double,lng2:Double):Flowable<List<VehicleEntity>>

}