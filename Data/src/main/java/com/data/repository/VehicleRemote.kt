package com.data.repository

import com.data.model.VehicleEntity
import com.domain.interactor.signin.SignIn
import com.domain.model.signup.SignUp
import io.reactivex.Flowable


/**
 * @author Ehtisham haq
 * Interface for Vehicle Remote Data Source which will be retrofit in our case
 **/
interface VehicleRemote{
    fun getVehicles(): Flowable<List<VehicleEntity>>
    fun getVehicles( lat1: Double,
                     lng1: Double,
                     lat2: Double,
                     lng2: Double): Flowable<List<VehicleEntity>>

    fun signUP(
        password: String,
        phone: String,
        lastName: String,
        firstName: String,
        email: String,
        confirmPassword: String):Flowable<SignUp>

    fun signIn(email: String,
               password: String
               ):Flowable<com.domain.model.signin.SignIn>

}