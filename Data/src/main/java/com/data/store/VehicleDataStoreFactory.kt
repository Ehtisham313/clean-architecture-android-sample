package com.data.store

import com.data.repository.VehicleDataStore
import javax.inject.Inject

/**
 * @author Ehtisham haq
 * Factory class to provide the data store reference
 **/
open class VehicleDataStoreFactory @Inject constructor(
    val mVechileCacheDataStore: VehicleCacheDataStore,
    val mVechileRemoteDataStore: VehicleRemoteDataStore
) {

    open fun getDataStore(
        vehiclesCached: Boolean,
        cacheExpired: Boolean
    ): VehicleDataStore {
        return if (vehiclesCached && !cacheExpired) {
            mVechileCacheDataStore
        } else {
            mVechileRemoteDataStore
        }
    }

    open fun getCacheDataStore(): VehicleDataStore {
        return mVechileCacheDataStore
    }

    open fun getRemoteDataStore(): VehicleDataStore {
        return mVechileRemoteDataStore
    }

}