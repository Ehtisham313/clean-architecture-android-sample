package com.data.store

import com.data.model.VehicleEntity
import com.data.repository.VehicleDataStore
import com.data.repository.VehicleRemote
import com.domain.model.signin.SignIn
import com.domain.model.signup.SignUp
import io.reactivex.Completable
import io.reactivex.Flowable
import java.lang.UnsupportedOperationException
import javax.inject.Inject

/**
* @author Ehtisham haq
 * The class can only provide the get vehicle operation for now
 * All other operations are part of local Cache Module
 **/

open class VehicleRemoteDataStore @Inject constructor(val mVehicleRemote: VehicleRemote):VehicleDataStore{

    override fun getVehiclesInBound(
        lat1: Double,
        lng1: Double,
        lat2: Double,
        lng2: Double
    ): Flowable<List<VehicleEntity>> {
        return mVehicleRemote.getVehicles( lat1,
            lng1,
            lat2,
            lng2)
    }


    override fun getVehicles(): Flowable<List<VehicleEntity>> {
        return mVehicleRemote.getVehicles()
    }

    override fun clearVehicles(): Completable {
        throw UnsupportedOperationException("Operation Not Supported Here")
    }

    override fun saveVehicles(vehicles: List<VehicleEntity>): Completable {
        throw UnsupportedOperationException("Operation Not Supported Here")
    }


    override fun getSelectedVehicle(): Flowable<VehicleEntity> {
        throw UnsupportedOperationException("Operation Not Supported Here")
    }

    override fun selectVehicle(vehicleId: Int): Completable {
        throw UnsupportedOperationException("Operation Not Supported Here")
    }

    override fun unSelectVehicle(vehicleId: Int): Completable {
        throw UnsupportedOperationException("Operation Not Supported Here")
    }

    override fun signUP(
        password: String,
        phone: String,
        lastName: String,
        firstName: String,
        email: String,
        confirmPassword: String
    ): Flowable<SignUp> {
        return mVehicleRemote.signUP(password,
            phone,
            lastName,
            firstName,
            email,
            confirmPassword)
    }

    override fun signIn(email: String, password: String): Flowable<SignIn> {
        return mVehicleRemote.signIn(email,password)
    }

}