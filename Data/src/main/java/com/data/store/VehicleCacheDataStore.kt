package com.data.store

import com.data.model.VehicleEntity
import com.data.repository.VehicleCache
import com.data.repository.VehicleDataStore
import com.domain.model.signin.SignIn
import com.domain.model.signup.SignUp
import io.reactivex.Completable
import io.reactivex.Flowable
import java.lang.IllegalArgumentException
import java.lang.UnsupportedOperationException
import javax.inject.Inject

/**
 * @author Ehtisham haq
 * Class to provide vehicle Cache Data Store
 **/

open class VehicleCacheDataStore @Inject constructor(val mVehicleCache: VehicleCache):VehicleDataStore{


    override fun clearVehicles(): Completable {
        return mVehicleCache.clearVehicles()
    }

    override fun saveVehicles(vehicles: List<VehicleEntity>): Completable {
        return mVehicleCache.saveVehicles(vehicles).andThen(mVehicleCache.setLastCacheTime(System.currentTimeMillis()))
    }

    override fun getVehicles(): Flowable<List<VehicleEntity>> {
        return mVehicleCache.getVehicles()
    }

    override fun getSelectedVehicle(): Flowable<VehicleEntity> {
        return mVehicleCache.getSelectedVehicle()
    }

    override fun selectVehicle(vehicleId: Int): Completable {
        return mVehicleCache.selectVehicle(vehicleId)
    }

    override fun unSelectVehicle(vehicleId: Int): Completable {
        return mVehicleCache.unSelectVehicle(vehicleId)
    }

    override fun signUP(
        password: String,
        phone: String,
        lastName: String,
        firstName: String,
        email: String,
        confirmPassword: String
    ): Flowable<SignUp> {
        throw UnsupportedOperationException("Operation Not Supported Here")
    }

    override fun signIn(email: String, password: String): Flowable<SignIn> {
        throw UnsupportedOperationException("Operation Not Supported Here")
    }

    override fun getVehiclesInBound(lat1: Double, lng1: Double, lat2: Double, lng2: Double): Flowable<List<VehicleEntity>> {
        return mVehicleCache.getVehiclesInBound(lat1,lng1,lat2,lng2)
    }


}