package com.data.model

class VehicleEntity (val id:Int,
                     val latitude:Double,val longitude:Double,
                     val fleetType:String,val heading:Double)