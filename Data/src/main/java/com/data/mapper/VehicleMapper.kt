package com.data.mapper

import com.data.model.VehicleEntity
import com.domain.model.Vehicle
import javax.inject.Inject


open class VehicleMapper @Inject constructor() : EntityMapper<VehicleEntity, Vehicle> {

    override fun mapFromEntity(entity: VehicleEntity): Vehicle {
        return Vehicle(entity.id,entity.latitude,entity.longitude,entity.fleetType,entity.heading)
    }

    override fun mapToEntity(domain: Vehicle): VehicleEntity {
        return VehicleEntity(domain.id,domain.latitude,domain.longitude,domain.fleetType,domain.heading)
    }

}