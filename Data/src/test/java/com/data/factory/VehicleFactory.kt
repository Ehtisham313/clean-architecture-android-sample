package com.data.factory

import com.data.model.VehicleEntity
import com.domain.model.Vehicle

object VehicleFactory {

    fun makeVehicleEntity(): VehicleEntity {
        return VehicleEntity(DataFactory.randomInt(),DataFactory.randomDouble(),
            DataFactory.randomDouble(),DataFactory.randomFleetType(),
            DataFactory.randomDouble())
    }

    fun makeVehicle(): Vehicle {
        return Vehicle(DataFactory.randomInt(),DataFactory.randomDouble(),
            DataFactory.randomDouble(),DataFactory.randomFleetType(),
            DataFactory.randomDouble())
    }


}