package com.data.mapper

import com.data.factory.VehicleFactory
import com.data.model.VehicleEntity
import com.domain.model.Vehicle
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class VehicleMapperTest {

    private val mapper = VehicleMapper()

    @Test
    fun mapFromEntityMapsData() {
        val entity = VehicleFactory.makeVehicleEntity()
        val model = mapper.mapFromEntity(entity)
        assertEqualData(entity, model)
    }

    @Test
    fun mapToEntityMapsData() {
        val model = VehicleFactory.makeVehicle()
        val entity = mapper.mapToEntity(model)

        assertEqualData(entity, model)
    }

    private fun assertEqualData(entity: VehicleEntity,
                                model: Vehicle
    ) {
        assertEquals(entity.id, model.id)
        assertEquals(entity.latitude, model.latitude)
        assertEquals(entity.longitude, model.longitude)
        assertEquals(entity.fleetType, model.fleetType)
        assertEquals(entity.heading, model.heading)
    }

}