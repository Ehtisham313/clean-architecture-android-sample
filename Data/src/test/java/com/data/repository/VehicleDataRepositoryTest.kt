package com.data.repository

import com.data.VehiclesDataRepository
import com.data.factory.DataFactory
import com.data.factory.VehicleFactory
import com.data.mapper.VehicleMapper
import com.data.model.VehicleEntity
import com.data.store.VehicleDataStoreFactory
import com.domain.model.Vehicle
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class VehicleDataRepositoryTest {

    private val mapper = mock<VehicleMapper>()
    private val factory = mock<VehicleDataStoreFactory>()
    private val store = mock<VehicleDataStore>()
    private val cache = mock<VehicleCache>()
    private val repository = VehiclesDataRepository(mapper, cache, factory)


    @Before
    fun setup() {
        stubFactoryGetDataStore()
        stubFactoryGetCacheDataStore()
        stubFactoryGetRemoteDataStore()
        stubIsCacheExpired(Single.just(false))
        stubIsCacheExpired(Single.just(false))
        stubAreVehiclesCached(Single.just(false))
        stubSaveVehicles(Completable.complete())
    }


    @Test
    fun getVehiclesCompletes() {
        stubGetVehicles(Flowable.just(listOf(VehicleFactory.makeVehicleEntity())))
        stubMapper(VehicleFactory.makeVehicle(), any())

        val testObserver = repository.getVehicles().test()
        testObserver.assertComplete()
    }

    @Test
    fun getVehiclesReturnsData() {
        val vehicleEntity = VehicleFactory.makeVehicleEntity()
        val vehicle = VehicleFactory.makeVehicle()
        stubGetVehicles(Flowable.just(listOf(vehicleEntity)))
        stubMapper(vehicle, vehicleEntity)

        val testObserver = repository.getVehicles().test()
        testObserver.assertValue(listOf(vehicle))
    }

    @Test
    fun getSelectedVehiclesInBoundCompletes() {
        stubGetVehiclesInBound(Flowable.just(listOf(VehicleFactory.makeVehicleEntity())))
        stubMapper(VehicleFactory.makeVehicle(), any())
        val testObserver = repository.getVehiclesInBound(
            DataFactory.randomDouble(), DataFactory.randomDouble(),
            DataFactory.randomDouble(), DataFactory.randomDouble()
        ).test()
        testObserver.assertComplete()
    }


    @Test
    fun getSelectedVehiclesInBoundReturnsData() {
        var vehicleEntitiy= VehicleFactory.makeVehicleEntity()
        var vehicle=VehicleFactory.makeVehicle()
        stubGetVehiclesInBound(Flowable.just(listOf(vehicleEntitiy)))
        stubMapper(vehicle, vehicleEntitiy)
        val testObserver = repository.getVehiclesInBound(
            DataFactory.randomDouble(), DataFactory.randomDouble(),
            DataFactory.randomDouble(), DataFactory.randomDouble()
        ).test()
        testObserver.assertValue(listOf(vehicle))
    }



    @Test
    fun getSelectedVehicleCompletes() {
        val vehicleEntity = VehicleFactory.makeVehicleEntity()
        val vehicle = VehicleFactory.makeVehicle()
        stubGetSelectedVehicle(Flowable.just(vehicleEntity))
        stubMapper(vehicle, vehicleEntity)

        val testObserver = repository.getSelectedVehicle().test()
        testObserver.assertComplete()
    }

    @Test
    fun getSelectedVehiclessReturnsData() {
        val vehicleEntity = VehicleFactory.makeVehicleEntity()
        val vehicle = VehicleFactory.makeVehicle()
        stubGetSelectedVehicle(Flowable.just(vehicleEntity))
        stubMapper(vehicle, vehicleEntity)

        val testObserver = repository.getSelectedVehicle().test()
        testObserver.assertValue(vehicle)
    }

    @Test
    fun selectVehicleCompletes() {
        stubSelectVehicle(Completable.complete())

        val testObserver = repository.selectVehicle(DataFactory.randomInt()).test()
        testObserver.assertComplete()
    }

    @Test
    fun unSelectVehicleCompletes() {
        stubUnSelectVehicle(Completable.complete())

        val testObserver = repository.unSelectVehicle(DataFactory.randomInt()).test()
        testObserver.assertComplete()
    }

    private fun stubSelectVehicle(completable: Completable) {
        whenever(store.selectVehicle(any()))
            .thenReturn(completable)
    }

    private fun stubUnSelectVehicle(completable: Completable) {
        whenever(store.unSelectVehicle(any()))
            .thenReturn(completable)
    }


    private fun stubMapper(model: Vehicle, entity: VehicleEntity) {
        whenever(mapper.mapFromEntity(entity))
            .thenReturn(model)
    }

    private fun stubGetVehicles(observable: Flowable<List<VehicleEntity>>) {
        whenever(store.getVehicles())
            .thenReturn(observable)
    }

    private fun stubGetVehiclesInBound(flowable: Flowable<List<VehicleEntity>>) {
        whenever(store.getVehiclesInBound(any(), any(), any(), any()))
            .thenReturn(flowable)
    }

    private fun stubGetSelectedVehicle(observable: Flowable<VehicleEntity>) {
        whenever(store.getSelectedVehicle())
            .thenReturn(observable)
    }

    private fun stubFactoryGetDataStore() {
        whenever(factory.getDataStore(any(), any()))
            .thenReturn(store)
    }

    private fun stubFactoryGetCacheDataStore() {
        whenever(factory.getCacheDataStore())
            .thenReturn(store)
    }


    private fun stubFactoryGetRemoteDataStore() {
        whenever(factory.getRemoteDataStore())
            .thenReturn(store)
    }


    private fun stubIsCacheExpired(single: Single<Boolean>) {
        whenever(cache.isVehiclesCacheExpired())
            .thenReturn(single)
    }

    private fun stubAreVehiclesCached(single: Single<Boolean>) {
        whenever(cache.areVehiclesCached())
            .thenReturn(single)
    }

    private fun stubSaveVehicles(completable: Completable) {
        whenever(store.saveVehicles(any()))
            .thenReturn(completable)
    }
}