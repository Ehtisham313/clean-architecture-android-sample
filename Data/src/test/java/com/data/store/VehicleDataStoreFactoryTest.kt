package com.data.store

import com.data.repository.VehicleCache
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class VehicleDataStoreFactoryTest{

    private val cache = mock<VehicleCache>()
    private val cacheStore = mock<VehicleCacheDataStore>()
    private val remoteStore = mock<VehicleRemoteDataStore>()
    private val factory = VehicleDataStoreFactory(cacheStore, remoteStore)

    @Test
    fun getRemoteStoreRetrievesRemoteSource() {
        assert(factory.getRemoteDataStore() is VehicleRemoteDataStore)
    }

    @Test
    fun getCacheStoreRetrievesCacheSource() {
        assert(factory.getCacheDataStore() is VehicleCacheDataStore)
    }

    @Test
    fun getDataStoreReturnsRemoteSourceWhenNoCachedData() {
        assert(factory.getDataStore(false, false) is VehicleRemoteDataStore)
    }

    @Test
    fun getDataStoreReturnsRemoteSourceWhenCacheExpired() {
        assert(factory.getDataStore(false, false) is VehicleRemoteDataStore)
    }

    @Test
    fun getDataStoreReturnsCacheSourceWhenDataIsCached() {
        stubVehicleCacheAreVehicleCached(true)
       stubVehicleCacheIsVehicleCachedExpired(false)

        assert(factory.getDataStore(true, false) is VehicleCacheDataStore)
    }

    private fun stubVehicleCacheAreVehicleCached(areCached: Boolean) {
        whenever(cache.areVehiclesCached())
            .thenReturn(Single.just(areCached))
    }

    private fun stubVehicleCacheIsVehicleCachedExpired(expired: Boolean) {
        whenever(cache.isVehiclesCacheExpired())
            .thenReturn(Single.just(expired))
    }
    
    
}