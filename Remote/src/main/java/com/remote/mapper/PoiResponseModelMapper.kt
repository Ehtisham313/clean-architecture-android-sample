package com.remote.mapper

import com.data.model.VehicleEntity
import com.remote.model.PoiListItem
import javax.inject.Inject

open class PoiResponseModelMapper @Inject constructor():ModelMapper<PoiListItem,VehicleEntity>{
    override fun mapFromModel(model: PoiListItem): VehicleEntity {
        return VehicleEntity(model.id,model.coordinate.latitude,
                             model.coordinate.longitude,
                             model.fleetType,model.heading)
    }

}