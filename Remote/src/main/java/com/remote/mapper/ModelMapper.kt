package com.remote.mapper


/**
 * @author Ehtisham haq
 * The Interface contain an abstract method
 * which will take Model as input and return it into Model for Data Layer
 **/
interface ModelMapper<in M,out E>{

    fun mapFromModel(model:M):E
}