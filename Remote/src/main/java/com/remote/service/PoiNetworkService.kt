package com.remote.service

import com.domain.model.signin.SignIn
import com.domain.model.signup.SignUp
import com.remote.model.PoiResponseModel
import com.remote.model.requests.SignInRequest
import com.remote.model.requests.SignUpRequest
import io.reactivex.Flowable
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface PoiNetworkService {

    @GET("/")
    fun getVehicleList(@Query("p1Lat") p1Lat:Double,@Query("p1Lon") p1Lon:Double,
                       @Query("p2Lat") p2Lat:Double,@Query("p2Lon") p2Lon:Double) : Flowable<PoiResponseModel>

    @POST("/api/auth/signup")
    fun signUP(@Body signUpRequest: SignUpRequest):Flowable<SignUp>

    @POST("/api/auth/login")
    fun signIN(@Body signInRequest: SignInRequest):Flowable<SignIn>

}