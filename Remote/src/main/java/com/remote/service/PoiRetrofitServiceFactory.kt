package com.remote.service

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.util.concurrent.TimeUnit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.Gson

/**
 * @author Ehtisham haq
 * The class will declare all the functions required to build retrofit client
 * It will instianate the retrofit service for network calls
 **/

object PoiRetrofitServiceFactory {


    /**
     * Method to access retrofit service provided by
     * @see makePoiRetrofitService
     * @param isDebug it will be used in logging interceptor
     **/
    fun makePoiRetrofitService(isDebug: Boolean): PoiNetworkService {
        val okHttpClient = makeOkHttpClient(
            makeLoggingInterceptor((isDebug))
        )
        return makePoiRetrofitService(okHttpClient, Gson())
    }

    /**
     * Method will provide Retrofit Service
     * @param okHttpClient it will be provided by
     * @see makeOkHttpClient
     **/
    private fun makePoiRetrofitService(okHttpClient: OkHttpClient, gson: Gson): PoiNetworkService {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://192.168.60.135/")
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        return retrofit.create(PoiNetworkService::class.java)
    }

    /**
     * Method will provide OKhttp client
     * @param httpLoggingInterceptor will be provided by
     * @see makeLogginInterceptor method
     * For Now we have set connection timeout and readtimeout and logging interceptor
     **/
    private fun makeOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .build()
    }

    /**
     * It will return OkHttp Loggin Interceptor
     * We have three types of interceptors
     * @see HttpLoggingInterceptor
     * @param isDebug if it is true the app will print all logs else no logs will be printed
     **/
    private fun makeLoggingInterceptor(isDebug: Boolean): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if (isDebug) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
        return logging
    }

}