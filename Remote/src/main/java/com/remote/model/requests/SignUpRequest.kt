package com.remote.model.requests

import com.google.gson.annotations.SerializedName

data class SignUpRequest(
	val password: String,
	val phone: String,
	@SerializedName("last_name")
	val lastName: String,
	@SerializedName("first_name")
	val firstName: String,
	val email: String,
	@SerializedName("confirm_password")
	val confirmPassword: String
)
