package com.remote.model.requests

data class SignInRequest(
	val password: String,
	val email: String
)
