package com.remote.model

import com.google.gson.annotations.SerializedName


data class PoiResponseModel(

	@field:SerializedName("poiList")
	val poiList: List<PoiListItem>
)