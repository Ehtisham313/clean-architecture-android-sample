package com.remote.model

import com.google.gson.annotations.SerializedName

data class Coordinate(

	@field:SerializedName("latitude")
	val latitude: Double,

	@field:SerializedName("longitude")
	val longitude: Double
)