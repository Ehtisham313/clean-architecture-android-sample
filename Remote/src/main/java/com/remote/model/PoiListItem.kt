package com.remote.model


import com.google.gson.annotations.SerializedName


data class PoiListItem(

	@field:SerializedName("coordinate")
	val coordinate: Coordinate,

	@field:SerializedName("fleetType")
	val fleetType: String,

	@field:SerializedName("heading")
	val heading: Double,

	@field:SerializedName("id")
	val id: Int
)