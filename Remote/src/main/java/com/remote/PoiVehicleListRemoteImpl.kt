package com.remote

import com.data.model.VehicleEntity
import com.data.repository.VehicleRemote
import com.domain.model.signin.SignIn
import com.domain.model.signup.SignUp
import com.remote.mapper.PoiResponseModelMapper
import com.remote.model.requests.SignInRequest
import com.remote.model.requests.SignUpRequest
import com.remote.service.PoiNetworkService
import io.reactivex.Flowable
import javax.inject.Inject

class PoiVehicleListRemoteImpl @Inject constructor(
    private val service: PoiNetworkService,
    private val mapper: PoiResponseModelMapper
) : VehicleRemote {
    override fun getVehicles(
        lat1: Double,
        lng1: Double,
        lat2: Double,
        lng2: Double
    ): Flowable<List<VehicleEntity>> {
        return service.getVehicleList(lat1, lng1, lat2, lng2)
            .map {
                it.poiList.map { mapper.mapFromModel(it) }
            }

    }

    override fun signUP(
        password: String,
        phone: String,
        lastName: String,
        firstName: String,
        email: String,
        confirmPassword: String
    ): Flowable<SignUp> {
        return service.signUP(
            SignUpRequest(
                password,
                phone,
                lastName,
                firstName,
                email,
                confirmPassword
            )
        )
    }

    override fun signIn(email: String, password: String): Flowable<SignIn> {
        return service.signIN(SignInRequest(password,email))
    }


    override fun getVehicles(): Flowable<List<VehicleEntity>> {
        return service.getVehicleList(53.694865, 9.757589, 53.394655, 10.099891)
            .map {
                it.poiList.map { mapper.mapFromModel(it) }
            }
    }


}