package com.remote.factory

import com.data.model.VehicleEntity
import com.remote.model.Coordinate
import com.remote.model.PoiListItem
import com.remote.model.PoiResponseModel

object VehicleDataFactory {

    fun makeCordinates(): Coordinate {
        return Coordinate(DataFactory.randomDouble(), DataFactory.randomDouble())
    }

    fun makePoiListItem(): PoiListItem {
        return PoiListItem(
            makeCordinates(),
            DataFactory.randomFleetType(),
            DataFactory.randomDouble(),
            DataFactory.randomInt()
        )
    }

    fun makePoiListResponse(): PoiResponseModel {
        return PoiResponseModel(makePoiList(2))
    }

    fun makePoiList(count: Int): List<PoiListItem> {
        val items = mutableListOf<PoiListItem>()
        repeat(count) {
            items.add(makePoiListItem())
        }
        return items
    }

    fun makeVehicleEntity(): VehicleEntity {
        return VehicleEntity(
            DataFactory.randomInt(), DataFactory.randomDouble(),
            DataFactory.randomDouble(), DataFactory.randomFleetType(),
            DataFactory.randomDouble())
    }


}