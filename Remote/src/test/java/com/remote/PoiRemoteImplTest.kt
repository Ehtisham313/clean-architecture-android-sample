package com.remote

import com.data.model.VehicleEntity
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.remote.factory.VehicleDataFactory
import com.remote.mapper.PoiResponseModelMapper
import com.remote.model.PoiListItem
import com.remote.model.PoiResponseModel
import com.remote.service.PoiNetworkService
import io.reactivex.Flowable
import io.reactivex.Observable
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
class PoiRemoteImplTest {

    private val mapper = mock<PoiResponseModelMapper>()
    private val service = mock<PoiNetworkService>()
    private val remote = PoiVehicleListRemoteImpl(service, mapper)

    @Test
    fun getVehiclesCompletes() {
        stubPoiServiceGetVehicles(
            Flowable.just(
                VehicleDataFactory.makePoiListResponse()
            )
        )
        stubVehicleResponseModelMapperMapFromModel(
            any(),
            VehicleDataFactory.makeVehicleEntity()
        )

        val testObserver = remote.getVehicles().test()
        testObserver.assertComplete()
    }



    @Test
    fun getVehiclesCallsServer() {
        stubPoiServiceGetVehicles(
            Flowable.just(
                VehicleDataFactory.makePoiListResponse()
            )
        )
        stubVehicleResponseModelMapperMapFromModel(
            any(),
            VehicleDataFactory.makeVehicleEntity()
        )

        remote.getVehicles().test()
        verify(service).getVehicleList(any(), any(), any(),any())
    }


    @Test
    fun getVehiclesReturnsData() {
        val response = VehicleDataFactory.makePoiListResponse()
        stubPoiServiceGetVehicles(Flowable.just(response))
        val entities = mutableListOf<VehicleEntity>()
        response.poiList.forEach {
            val entity = VehicleDataFactory.makeVehicleEntity()
            entities.add(entity)
            stubVehicleResponseModelMapperMapFromModel(it, entity)
        }
        val testObserver = remote.getVehicles().test()
        testObserver.assertValue(entities)
    }

    @Test
    fun getVehiclesCallsServerWithCorrectParameters() {
        stubPoiServiceGetVehicles(
            Flowable.just(
                VehicleDataFactory.makePoiListResponse()
            )
        )
        stubVehicleResponseModelMapperMapFromModel(
            any(),
            VehicleDataFactory.makeVehicleEntity()
        )

        remote.getVehicles().test()
        verify(service).getVehicleList(53.694865, 9.757589, 53.394655, 10.099891)
    }




    private fun stubPoiServiceGetVehicles(
        observable:
        Flowable<PoiResponseModel>
    ) {
        whenever(service.getVehicleList(any(), any(), any(), any()))
            .thenReturn(observable)
    }

    private fun stubVehicleResponseModelMapperMapFromModel(
        model: PoiListItem,
        entity: VehicleEntity
    ) {
        whenever(mapper.mapFromModel(model))
            .thenReturn(entity)
    }


}