package com.remote.mapper

import com.remote.factory.VehicleDataFactory
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class PoiResponseModelMapperTest {
    private val mapper = PoiResponseModelMapper()
    @Test
    fun mapFromModelMapsData() {
        val model = VehicleDataFactory.makePoiListItem()
        val entity = mapper.mapFromModel(model)

        assertEquals(model.id, entity.id)
        assertEquals(model.fleetType, entity.fleetType)
        assertEquals(model.coordinate.latitude, entity.latitude)
        assertEquals(model.coordinate.longitude, entity.longitude)
        assertEquals(model.heading, entity.heading)
    }
}