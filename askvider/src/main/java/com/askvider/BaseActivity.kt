package com.askvider

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import dagger.android.AndroidInjection
import dagger.android.support.DaggerAppCompatActivity


/**
 *  @author Ehtisham haq
 *  BaseActivity for activities,
 *  the purpose to make base is to avoid boiler plate in child classes.
 *  We have generic ViewDataBinding Class type defined so we could set it.
 *
 **/
abstract class BaseActivity<T:ViewDataBinding>: DaggerAppCompatActivity() {

    lateinit var mViewDataBinding:T


    /**
     * A dialog showing a progress indicator and an optional text message or
     * view.
     */
    protected lateinit var mProgressDialog: AlertDialog.Builder
    protected lateinit var dialog:Dialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        mViewDataBinding=DataBindingUtil.setContentView(this,setLayoutId())
        init(savedInstanceState)
    }


    abstract fun setLayoutId():Int

    abstract fun init(savedInstanceState: Bundle?)

    override fun onPause() {
        dismiss()
        super.onPause()
    }

    /**
     *  Overriding method to get user touch event.
     **/

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        val v = currentFocus
        if (v is EditText) {
            val scoops = IntArray(2)
            v.getLocationOnScreen(scoops)
            val x = event.rawX + v.left - scoops[0]
            val y = event.rawY + v.top - scoops[1]

            if (event.action == MotionEvent.ACTION_UP && (x < v.left || x >= v.right || y < v.top || y > v
                    .bottom)
            ) {
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(
                    v
                        .windowToken, 0
                )
            }
        }
        return super.dispatchTouchEvent(event)
    }

    fun showLoading(){
        if(!this::mProgressDialog.isInitialized){
            mProgressDialog=AlertDialog.Builder(this)
            mProgressDialog.setView(R.layout.progress_dialoge)
            dialog=mProgressDialog.create()

        }
        dialog.show()
    }


    fun dismiss(){
        if(this::mProgressDialog.isInitialized){
            dialog.dismiss()
        }
    }
}