package com.askvider

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

/**
 * @author Ehtisham haq
 * Base Fragment Classes for all Base Related Operations
 **/
abstract class BaseFragment<F : ViewDataBinding> : Fragment() {


    private val mShouldSave = true
    private var mIsInLeft: Boolean = false
    private var mIsOutLeft: Boolean = false
    protected var mIsCurrentScreen: Boolean = false
    private var mIsPush: Boolean = false

    private var mInitialized = true
    private var mViewCreated = false
    private var mViewDestroyed = false

    protected lateinit var mFragmentViewDataBinding:F

    @LayoutRes
    protected abstract fun getLayoutId(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mFragmentViewDataBinding = DataBindingUtil.inflate<F>(inflater, getLayoutId(), container, false)
        return mFragmentViewDataBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onVisible(view, savedInstanceState)
        mViewCreated = true
        mViewDestroyed = false
    }


    override fun onDestroyView() {
        mViewDestroyed = true
        mViewCreated = false
        onInVisible()
        mFragmentViewDataBinding.unbind()
        super.onDestroyView()
    }

    fun showLoading() {
        (activity as BaseActivity<*>).showLoading()
    }

    fun hideLoading() {
        (activity as BaseActivity<*>).dismiss()
    }

    fun isInitialized(): Boolean {
        return mInitialized
    }

    fun initialized() {
        mInitialized = false
    }

    fun isViewCreated(): Boolean {
        return mViewCreated
    }

    fun setInLeft(inLeft: Boolean) {
        mIsInLeft = inLeft
    }

    fun setOutLeft(outLeft: Boolean) {
        mIsOutLeft = outLeft
    }

    fun setCurrentScreen(currentScreen: Boolean) {
        mIsCurrentScreen = currentScreen
    }

    fun setPush(push: Boolean) {
        mIsPush = push
    }

    fun isShouldSave(): Boolean {
        return mShouldSave
    }

    protected abstract fun onVisible(view: View, savedInstanceState: Bundle?)

    protected fun handleAfterVisible() {}

    protected abstract fun onInVisible()


    /*
     * Methods to get Animations From Anim Folder
     */

    protected fun getLeftInAnimId(): Int {
        return R.anim.slide_in_left
    }

    protected fun getRightInAnimId(): Int {
        return R.anim.slide_in_right
    }

    protected fun getLeftOutAnimId(): Int {
        return R.anim.slide_out_left
    }

    protected fun getRightOutAnimId(): Int {
        return R.anim.slide_out_right
    }

    fun dismiss(){
        (activity as BaseActivity<*>).dismiss()
    }




}