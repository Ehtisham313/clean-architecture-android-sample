package com.askvider.mapper

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.*
import com.askvider.R
import com.presentation.model.VehicleView
import javax.inject.Inject

/**
 * @author Ehtisham haq
 * Class to Map Objects into Markers
 **/
public class MapMarkerMapper @Inject constructor(val context: Context): ViewMapper<VehicleView, MarkerOptions> {



    override fun mapToView(presentation: VehicleView): MarkerOptions {
        return MarkerOptions()
                .position(LatLng(presentation.latitude,presentation.longitude))
                .title(presentation.id.toString()).icon(
                if(presentation.fleetType=="TAXI")  bitmapDescriptorFromVector(context,R.drawable.ic_taxicar) else bitmapDescriptorFromVector(context,R.drawable.ic_poolingcar))
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }

}