package com.askvider.mapper

import com.askvider.model.Vehicle
import com.presentation.model.VehicleView
import javax.inject.Inject


/**
 * @author Ehtisham haq
 * Class to Map Objects into Vehicle Model
 **/
class VehicleViewMapper @Inject constructor(): ViewMapper<VehicleView, Vehicle> {

    override fun mapToView(presentation: VehicleView): Vehicle {
        return Vehicle(presentation.id,
                       presentation.latitude,presentation.longitude,
                       presentation.fleetType,presentation.heading)
    }

}