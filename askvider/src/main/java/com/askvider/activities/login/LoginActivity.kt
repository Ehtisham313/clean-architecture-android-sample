package com.askvider.activities.login

import android.os.Bundle
import com.askvider.BaseActivity
import com.askvider.R
import com.askvider.databinding.ActivityLoginBinding

class LoginActivity :BaseActivity<ActivityLoginBinding>(){

    override fun setLayoutId(): Int = R.layout.activity_login

    override fun init(savedInstanceState: Bundle?) {
    }

}