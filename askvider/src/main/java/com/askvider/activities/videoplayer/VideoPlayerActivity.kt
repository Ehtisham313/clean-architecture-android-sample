package com.askvider.activities.videoplayer

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.Surface
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import com.askvider.BaseActivity
import com.askvider.R
import com.askvider.activities.main.MainActivity
import com.askvider.databinding.VideoPlayerActivityBinding
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import dagger.android.AndroidInjection

class VideoPlayerActivity:BaseActivity<VideoPlayerActivityBinding>(){

    companion object{
        fun start(context: Context){
            val intent= Intent(context, VideoPlayerActivity::class.java)
            context.startActivity(intent);
        }
    }

    override fun setLayoutId(): Int = R.layout.video_player_activity

    override fun onCreate(savedInstanceState: Bundle?) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        mViewDataBinding= DataBindingUtil.setContentView(this,setLayoutId())
        init(savedInstanceState)
    }

    override fun init(savedInstanceState: Bundle?) {
       if(getWindowManager().getDefaultDisplay().getRotation()== Surface.ROTATION_0){
           window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
       }else{
           window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
       }
       window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
       setPlayer()
    }

    fun setPlayer(){
        var url="https://www.askvider.com/banner/home.mp4"
        lateinit var player: SimpleExoPlayer
        player = ExoPlayerFactory.newSimpleInstance(this)
        val dataSourceFactory = DefaultDataSourceFactory(
            this,
            Util.getUserAgent(this, "com.askvider")
        )

        val videoSource = ExtractorMediaSource.Factory(dataSourceFactory)
            .createMediaSource(Uri.parse(url))

        player.prepare(videoSource)
        player.playWhenReady = true
        mViewDataBinding.vadBanner.hideController()
        mViewDataBinding.vadBanner.setPlayer(player)
        mViewDataBinding.vadBanner.hideController()
        mViewDataBinding.vadBanner.setShowBuffering(true)
        player.volume = 0f
        player.repeatMode = Player.REPEAT_MODE_ALL
    }

}