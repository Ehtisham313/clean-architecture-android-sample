package com.askvider.activities.splash

import android.os.Bundle
import com.askvider.R
import com.askvider.databinding.ActivitySplashBinding
import android.os.CountDownTimer
import com.askvider.BaseActivity
import com.askvider.activities.main.MainActivity


class SplashActivity :BaseActivity<ActivitySplashBinding>(){

    override fun setLayoutId(): Int =R.layout.activity_splash

    override fun init(savedInstanceState: Bundle?) {
        object : CountDownTimer(3000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                MainActivity.start(this@SplashActivity)
            }

        }.start()

    }




}