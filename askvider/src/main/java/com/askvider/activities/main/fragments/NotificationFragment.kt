package com.askvider.activities.main.fragments

import android.os.Bundle
import android.view.View
import com.askvider.BaseFragment
import com.askvider.R
import com.askvider.databinding.FragmentNotificationBinding

class NotificationFragment:BaseFragment<FragmentNotificationBinding>(){

    override fun getLayoutId(): Int = R.layout.fragment_notification

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
    }

    override fun onInVisible() {
    }


}