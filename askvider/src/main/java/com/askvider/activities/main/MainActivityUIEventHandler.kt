package com.askvider.activities.main

import androidx.databinding.BaseObservable
import androidx.databinding.library.baseAdapters.BR
import javax.inject.Inject

/**
 * @author Ehtisham haq
 * Enum to define the State
 **/
enum class SelectedTab {
    HOME, NOTIFICATION, CHAT, PROFILE, VAD
}

class MainActivityUIEventHandler @Inject constructor():BaseObservable(){

    var isHomeSelected=true
    var isNotificationSelected=false
    var isChatSelected=false
    var isVadSelected=false
    var isProfileSelected=false


    fun setSelected(selected:SelectedTab){

        if(SelectedTab.HOME==selected) isHomeSelected=true else isHomeSelected=false

        if(SelectedTab.NOTIFICATION==selected) isNotificationSelected=true else isNotificationSelected=false

        if(SelectedTab.CHAT==selected) isChatSelected=true else isChatSelected=false

        if(SelectedTab.VAD==selected) isVadSelected=true else isVadSelected=false

        if(SelectedTab.PROFILE==selected) isProfileSelected=true else isProfileSelected=false

        notifyPropertyChanged(BR._all)

    }




}