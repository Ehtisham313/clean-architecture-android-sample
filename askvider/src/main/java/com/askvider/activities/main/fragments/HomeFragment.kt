package com.askvider.activities.main.fragments

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.askvider.BaseFragment
import com.askvider.R
import com.askvider.activities.main.MainActivity
import com.askvider.activities.videoplayer.VideoPlayerActivity
import com.askvider.adapter.home.ChannelsAdapter
import com.askvider.adapter.home.RecentListingAdapter
import com.askvider.databinding.FragmentHomeBinding
import com.askvider.injection.ViewModelFactory
import com.askvider.model.Vehicle
import com.presentation.SignUpViewModel
import javax.inject.Inject

class HomeFragment @Inject constructor():BaseFragment<FragmentHomeBinding>(){

    lateinit var player: SimpleExoPlayer

    @Inject lateinit var channelsAdapter: ChannelsAdapter
    @Inject lateinit var recentListingAdapter: RecentListingAdapter

    @Inject lateinit var viewModelFactory:ViewModelFactory

    private lateinit var signUpViewModel: SignUpViewModel


    override fun getLayoutId(): Int= R.layout.fragment_home

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        setUpExoplayer()
        setupAdapters()

        signUpViewModel = ViewModelProviders.of(activity as MainActivity, viewModelFactory)
            .get(SignUpViewModel::class.java)

        signUpViewModel.signIn()

    }


    fun setupAdapters(){
            mFragmentViewDataBinding.contentTag.channelAdapter.apply {
                adapter=channelsAdapter
                layoutManager=LinearLayoutManager(context!!,LinearLayoutManager.HORIZONTAL,false)
            }

            mFragmentViewDataBinding.contentTag.recentListingRV.apply {
                adapter=recentListingAdapter
                layoutManager=GridLayoutManager(context!!,2)
            }

            channelsAdapter.updateList(setupDummy())
            recentListingAdapter.updateList(setupDummy())

            recentListingAdapter.recentListListener=object: RecentListingAdapter.RecentListingListener{
                override fun onPictureClicked(vehicleId: Vehicle) {
                    VideoPlayerActivity.start(context!!)
                }
            }

    }

    fun setupDummy():ArrayList<Vehicle>{

        var myFakeList=ArrayList<Vehicle>()
        myFakeList.add(Vehicle(1,1.0,1.0,"",2.0))
        myFakeList.add(Vehicle(1,1.0,1.0,"",2.0))
        myFakeList.add(Vehicle(1,1.0,1.0,"",2.0))
        myFakeList.add(Vehicle(1,1.0,1.0,"",2.0))
        myFakeList.add(Vehicle(1,1.0,1.0,"",2.0))
        myFakeList.add(Vehicle(1,1.0,1.0,"",2.0))
        myFakeList.add(Vehicle(1,1.0,1.0,"",2.0))
        myFakeList.add(Vehicle(1,1.0,1.0,"",2.0))

        return myFakeList
    }

    fun setUpExoplayer(){

        player = ExoPlayerFactory.newSimpleInstance(activity!!)
        val dataSourceFactory = DefaultDataSourceFactory(
            activity,
            Util.getUserAgent(activity!!, "com.askvider")
        )

        val videoSource = ExtractorMediaSource.Factory(dataSourceFactory)
            .createMediaSource(Uri.parse("https://www.askvider.com/banner/home.mp4"))

        player.prepare(videoSource)
        player.playWhenReady = true

        mFragmentViewDataBinding.contentTag.playerView.hideController()
        mFragmentViewDataBinding.contentTag.playerView.setPlayer(player)
        player.volume = 0f
        player.repeatMode = Player.REPEAT_MODE_ALL

    }

    override fun onInVisible() {
    }


}