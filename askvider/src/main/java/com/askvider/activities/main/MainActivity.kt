package com.askvider.activities.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.*
import androidx.navigation.fragment.NavHostFragment
import com.askvider.BaseActivity
import com.askvider.R
import com.askvider.databinding.ActivityMainBinding
import com.askvider.handler.MainEventHandler
import com.askvider.injection.ViewModelFactory
import com.google.android.material.navigation.NavigationView
import com.presentation.GetVehiclesViewModel
import com.presentation.SignUpViewModel
import javax.inject.Inject

class MainActivity:BaseActivity<ActivityMainBinding>(),MainEventHandler{


    lateinit var mHost:NavHostFragment

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var signUpViewModel: SignUpViewModel


    @Inject
    lateinit var uiHandler:MainActivityUIEventHandler

    companion object{
        fun start(context: Context){
            val intent= Intent(context, MainActivity::class.java)
            context.startActivity(intent);
        }
    }



    override fun setLayoutId(): Int= R.layout.activity_main

    override fun init(savedInstanceState: Bundle?) {

        mViewDataBinding.apply {
            handler=this@MainActivity
            ui=uiHandler
        }

        signUpViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(SignUpViewModel::class.java)

      //  signUpViewModel.signIn()

        mHost = supportFragmentManager
            .findFragmentById(R.id.fragmentContainer) as NavHostFragment? ?: return

    }


    override fun onProfileClick() {
        val navController = findNavController(R.id.fragmentContainer)
        Navigation.setViewNavController(mViewDataBinding.bottomBar.accountIV, navController)
        mViewDataBinding.bottomBar.accountIV.findNavController().navigate(R.id.profile_navigation)
        uiHandler.setSelected(SelectedTab.PROFILE)
    }

    override fun onHomeClick() {
        val navController = findNavController(R.id.fragmentContainer)
        Navigation.setViewNavController(mViewDataBinding.bottomBar.homeIV, navController)
        mViewDataBinding.bottomBar.homeIV.findNavController().navigate(R.id.home_navigation)
        uiHandler.setSelected(SelectedTab.HOME)
    }

    override fun onChatClick() {
        val navController = findNavController(R.id.fragmentContainer)
        Navigation.setViewNavController(mViewDataBinding.bottomBar.chatIV, navController)
        mViewDataBinding.bottomBar.chatIV.findNavController().navigate(R.id.chat_navigation)
        uiHandler.setSelected(SelectedTab.CHAT)
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onNotificationClick() {
        val navController = findNavController(R.id.fragmentContainer)
        Navigation.setViewNavController(mViewDataBinding.bottomBar.notificationIV, navController)
        mViewDataBinding.bottomBar.notificationIV.findNavController().navigate(R.id.notification_navigation)
        uiHandler.setSelected(SelectedTab.NOTIFICATION)
    }

}
