package com.askvider.activities.vehicle

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.askvider.BaseActivity
import com.askvider.R
import com.askvider.databinding.ActivityVehicleNewBinding

class NewVehicleActivity:BaseActivity<ActivityVehicleNewBinding>(){

    companion object{
        fun start(context: Context){
            val intent= Intent(context,NewVehicleActivity::class.java)
            context.startActivity(intent);
        }
    }


    override fun setLayoutId(): Int= R.layout.activity_vehicle_new

    override fun init(savedInstanceState: Bundle?) {
    }

}