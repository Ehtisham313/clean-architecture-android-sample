package com.askvider.activities.vehicle.fragments

import android.os.Bundle
import android.view.View
import com.askvider.BaseFragment
import com.askvider.R
import com.askvider.databinding.FragmentListLayotBinding
import com.askvider.handler.VehicleUIHandler
import com.askvider.injection.ViewModelFactory
import com.askvider.mapper.VehicleViewMapper
import com.askvider.activities.vehicle.VehicleAdapter
import javax.inject.Inject

class VehicleListFragment:BaseFragment<FragmentListLayotBinding>(){

    @Inject
    lateinit var vehicleAdapter: VehicleAdapter


    @Inject
    lateinit var mapper: VehicleViewMapper

    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    @Inject
    lateinit var mVehiclesUIHandler: VehicleUIHandler



    override fun getLayoutId(): Int= R.layout.fragment_list_layot

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
    }

    override fun onInVisible() {
    }

}