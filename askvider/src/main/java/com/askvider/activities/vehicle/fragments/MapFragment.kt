package com.askvider.activities.vehicle.fragments

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.askvider.BaseFragment
import com.askvider.R
import com.askvider.databinding.FragmentMapLayoutBinding
import com.askvider.handler.VehicleEventHandler
import com.askvider.handler.VehicleUIHandler
import com.askvider.injection.ViewModelFactory
import com.askvider.mapper.MapMarkerMapper
import com.askvider.mapper.VehicleViewMapper
import com.askvider.model.Vehicle
import com.askvider.activities.vehicle.MapListeners
import com.askvider.activities.vehicle.VehicleListener
import com.presentation.GetVehiclesViewModel
import com.presentation.model.VehicleView
import com.presentation.state.Resource
import com.presentation.state.ResourceState
import timber.log.Timber
import javax.inject.Inject

public class MapFragment @Inject constructor()
                        : BaseFragment<FragmentMapLayoutBinding>(),
                          VehicleEventHandler, MapListeners {


    private lateinit var getVehicleViewModel: GetVehiclesViewModel


    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var mapMarkerMapper: MapMarkerMapper


    @Inject
    lateinit var mapper: VehicleViewMapper


    @Inject
    lateinit var mVehiclesUIHandler: VehicleUIHandler

    //Variable is used to check if map is at idle state
    private var isIdle = true


    private var mMap: GoogleMap? = null

    override fun getLayoutId(): Int = R.layout.fragment_map_layout

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        mFragmentViewDataBinding.map.onCreate(savedInstanceState)
        mFragmentViewDataBinding.map.getMapAsync(this)

        getVehicleViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(GetVehiclesViewModel::class.java)

        getVehicleViewModel.getVehicles().observe(this,
            Observer<Resource<List<VehicleView>>> {
                it?.let {
                    handleDataState(it)
                }
            })
        getVehicleViewModel.fetchVehicles()

    }

    override fun onInVisible() {

    }



    /**
     * Method to handle Data State
     * We are loading Data on Sucess
     */
    private fun handleDataState(resource: Resource<List<VehicleView>>) {
        when (resource.status) {
            ResourceState.SUCCESS -> {
                dismiss()
                resource.data?.let {
                    Timber.d(it.toString())
                    setMapMarkers(it)
                }
            }
            ResourceState.LOADING -> {
//                mVehiclesUIHandler.setLoadingViewVisibility(true)
                showLoading()
            }
            ResourceState.ERROR -> {
                dismiss()
//                mVehiclesUIHandler.setLoadingViewVisibility(false)
                Timber.e(resource.message)
            }
        }
    }





    /**
     * Intarface Callback Method on Vehicle Item Click
     */
    private val vehiclesListener = object : VehicleListener {
        override fun onVehicleClicked(vehicle: Vehicle) {
            zoomToSelectedMarker(vehicle)
        }

    }


    /**
     * Method to Handle Vehicle Item Click
     * We are Displaying Vehicle Data on UI and Also zooming in on map
     * Here we are collapsing our panel
     * Note in case of tablet device there will be no bottom sheet
     */
    fun zoomToSelectedMarker(currentVehicle: Vehicle) {

        mVehiclesUIHandler.setCurrentFleetType(currentVehicle.fleetType)
        mVehiclesUIHandler.setId(currentVehicle.id)
        mVehiclesUIHandler.setInfoViewShow(true)

        mMap?.let {
            val cameraPosition = CameraPosition.Builder()
                .target(
                    LatLng(currentVehicle.latitude, currentVehicle.longitude)
                )
                .zoom(it.maxZoomLevel - 5).build();
            it.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 2500, null)
        }
    }


    /**
     * Method to Populate Markers on Map
     */
    fun setMapMarkers(mVehicleList: List<VehicleView>) {
        try {
            val builder = LatLngBounds.Builder()
            mVehicleList.map {
                builder.include(mapMarkerMapper.mapToView(it).position)
                mMap?.addMarker(mapMarkerMapper.mapToView(it))
            }
            val bounds = builder.build()
            mMap?.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 0))

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    /**
     * Map State Listeners The Methods will be used to get Map bound Query database and get markers within bounds
     **/
    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        mMap?.let {
            it.setOnCameraIdleListener(this)
            it.setOnCameraMoveStartedListener(this)
            it.setOnCameraMoveListener(this)
            it.setOnCameraMoveCanceledListener(this)
        }
    }


    override fun onCameraMoveStarted(reasion: Int) {
        when (reasion) {
            GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE -> {
                if (isIdle) {
                    isIdle = false
                }
            }

        }
    }

    override fun onCameraMove() {
    }

    override fun onCameraMoveCanceled() {
    }

    override fun onCameraIdle() {
        if (!isIdle) {
            isIdle = true
            getVehiclesInMapBounds()
        }
    }


    /**
     * Method to Get Vehicles in Map Bounds
     */
    fun getVehiclesInMapBounds() {
        mMap?.let {
            val mCurrentLatLngBounds = it.getProjection().getVisibleRegion().latLngBounds
            getVehicleViewModel.fetchVehcilesInMapBound(
                mCurrentLatLngBounds.southwest.latitude,
                mCurrentLatLngBounds.southwest.longitude,
                mCurrentLatLngBounds.northeast.latitude,
                mCurrentLatLngBounds.northeast.longitude
            )
        }
    }

    /**
     * Method to Cancel Info View
     */
    override fun onCancelClick() {
        mVehiclesUIHandler.setInfoViewShow(false)
    }


    override fun onResume() {
        super.onResume()
        mFragmentViewDataBinding.map.onResume()
    }

    override fun onDestroyView() {
        mFragmentViewDataBinding.map.onDestroy()
        super.onDestroyView()
    }

    override fun onPause() {
        super.onPause()
        mFragmentViewDataBinding.map.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mFragmentViewDataBinding.map.onLowMemory()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mFragmentViewDataBinding.map.onSaveInstanceState(outState)
    }
}