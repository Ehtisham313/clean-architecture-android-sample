package com.askvider.activities.vehicle

import com.askvider.model.Vehicle


interface VehicleListener {

    fun onVehicleClicked(vehicleId: Vehicle)

}