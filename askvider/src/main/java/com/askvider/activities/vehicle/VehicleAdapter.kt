package com.askvider.activities.vehicle

import com.askvider.adapter.BaseAdapter
import com.askvider.model.Vehicle
import javax.inject.Inject
import android.graphics.drawable.Drawable
import androidx.databinding.BindingAdapter
import android.widget.ImageView
import com.askvider.R


/**
 * @author Ehtisham haq
 * Adapter class to display Vehicle List data
 * We are using Data binding to avoid boiler plate and reducing code size
 * Important since we are using vector drawables we need to make custom binding method to avoid crash on old devices
 **/

@BindingAdapter("app:srcCompat")
fun bindSrcCompat(imageView: ImageView, drawable: Drawable) {
    imageView.setImageDrawable(drawable)
}

class VehicleAdapter @Inject constructor():BaseAdapter(){

    var vehiclesList: List<Vehicle> = arrayListOf()
    var vehicleListener: VehicleListener? = null

    override fun getObjForPosition(position: Int): Any {
        return vehiclesList[position]
    }


    fun onItemClicked(selectedVehicle: Vehicle){
        vehicleListener?.onVehicleClicked(selectedVehicle)
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.item_vehicle
    }

    override fun getItemCount(): Int {
        return vehiclesList.count()
    }

    public fun updateList(mVehicleList:List<Vehicle>){
        vehiclesList=mVehicleList
        notifyDataSetChanged()
    }
}
