package com.askvider.activities.vehicle

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.askvider.injection.ViewModelFactory
import com.askvider.mapper.VehicleViewMapper
import com.presentation.GetVehiclesViewModel
import com.presentation.model.VehicleView
import com.presentation.state.Resource
import com.presentation.state.ResourceState
import javax.inject.Inject
import com.google.android.material.bottomsheet.BottomSheetBehavior
import androidx.annotation.NonNull
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.model.CameraPosition
import com.askvider.mapper.MapMarkerMapper
import com.askvider.model.Vehicle
import kotlinx.android.synthetic.main.layout_list.*
import com.google.android.gms.maps.model.LatLngBounds
import com.askvider.BaseActivity
import com.askvider.R
import com.askvider.databinding.ActivityVehicleBinding
import com.askvider.handler.VehicleEventHandler
import com.askvider.handler.VehicleUIHandler
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import timber.log.Timber


/**
 *  @author Ehtisham haq
 *  Main Class For Showing Vehicles Data and Vehicles on Map
 *  We are fetching all Vehicles within Hamsberg Bounds and then Displaying them on map
 *  ON Map Bound Change we are calling network api to fetch data within new buounds
 *  The Vehicle Event Handler interface will be used for callbacks of events called
 *  Map Listeners Interface implement all map call back methods
 *  @see MapListeners
 *  @see VehicleUIHandler
 **/

class VehicleActivity() : BaseActivity<ActivityVehicleBinding>(),
                          VehicleEventHandler, MapListeners,
                          HasSupportFragmentInjector {


    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>


    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return dispatchingAndroidInjector
    }



    companion object{

        fun start(context: Context){
            val intent=Intent(context,VehicleActivity::class.java)
            context.startActivity(intent);
        }
    }

    /**
     *  All of Varibales with Inject method will be injected by Dagger
     *  Any variable initialzed before AndroidInject.inject(this) will return us null value
     **/

    @Inject
    lateinit var vehicleAdapter: VehicleAdapter

    @Inject
    lateinit var mapper: VehicleViewMapper


    @Inject
    lateinit var mapMapper: MapMarkerMapper


    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    @Inject
    lateinit var mVehiclesUIHandler: VehicleUIHandler


    private lateinit var getVehicleViewModel: GetVehiclesViewModel


    private lateinit var sheetBehavior: BottomSheetBehavior<*>

    private var bottom_sheet: FrameLayout? = null

    private var mMap: GoogleMap? = null

    private lateinit var toolbar: Toolbar

    //Variable is used to check if map is at idle state
    private var isIdle = true


    //Variable is used to check if app is running on Tablet Device
    private var isTablet = false

    private val tag: String = VehicleActivity::class.java.simpleName




    override fun setLayoutId(): Int = R.layout.activity_vehicle

    /**
     * Main Method
     * We are initializing all the components here.
     * The class boiler plate code like Data Binding AndroidInjection could be moved to base abstract class
     **/
    override fun init(savedInstanceState: Bundle?) {
        mViewDataBinding.apply {
            handler=this.handler
            ui=mVehiclesUIHandler
        }

        isTablet = if (findViewById<View>(com.askvider.R.id.appbar) == null) true else false
        setToolbar()
        if (!isTablet) {
            setBottomSheet()
        }
        getVehicleViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(GetVehiclesViewModel::class.java)

        val mapFragment = supportFragmentManager
            .findFragmentById(com.askvider.R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        setupVehicleRecycler()


    }


    /**
     * Method to set Toolbar
     */
    fun setToolbar() {
        toolbar = findViewById<View>(com.askvider.R.id.toolbar) as Toolbar
        toolbar.title = resources.getString(com.askvider.R.string.app_name)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        toolbar.setNavigationOnClickListener {
            finish()
        }
    }


    /**
     * Method to set Bottom Sheet.
     * Note we will override toolbar click behivour to scroll down bottom sheet in this method
     */
    fun setBottomSheet() {
        bottom_sheet = findViewById<FrameLayout>(com.askvider.R.id.bottom_sheet_list_layout)

        sheetBehavior = BottomSheetBehavior.from<View>(bottom_sheet)
        sheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(@NonNull view: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }

            override fun onSlide(@NonNull view: View, v: Float) {

            }
        })


        toolbar.setNavigationOnClickListener {
            if (sheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            } else {
                finish()
            }

        }

    }


    /**
     * Method to set Bottom Sheet.
     * Calling For Vehicles Data on App Start
     */
    override fun onStart() {
        super.onStart()
        getVehicleViewModel.getVehicles().observe(this,
            Observer<Resource<List<VehicleView>>> {
                it?.let {
                    handleDataState(it)
                }
            })
        getVehicleViewModel.fetchVehicles()
    }


    /**
     * Method to set Vehicle View
     */
    fun setupVehicleRecycler() {
        vehicleAdapter.vehicleListener = vehiclesListener
        recycler_vehicles.apply {
            layoutManager = LinearLayoutManager(this@VehicleActivity)
            adapter = vehicleAdapter

        }
    }


    /**
     * Method to handle Data State
     * We are loading Data on Sucess
     */
    private fun handleDataState(resource: Resource<List<VehicleView>>) {
        when (resource.status) {
            ResourceState.SUCCESS -> {
                dismiss()
//                mVehiclesUIHandler.setLoadingViewVisibility(false)

                resource.data?.let {
                    Timber.d(it.toString())
                    handleSucessState(it.map { mapper.mapToView(it) })
                    setMapMarkers(it)
                }
            }
            ResourceState.LOADING -> {
//                mVehiclesUIHandler.setLoadingViewVisibility(true)
                showLoading()
            }
            ResourceState.ERROR -> {
                dismiss()
//                mVehiclesUIHandler.setLoadingViewVisibility(false)
                Timber.e(resource.message)
            }
        }
    }


    /**
     * Method to handle Data State
     * Method to Update List on Data Sucess
     */
    fun handleSucessState(resource: List<Vehicle>) {
        vehicleAdapter.updateList(resource)

    }


    /**
     * Intarface Callback Method on Vehicle Item Click
     */
    private val vehiclesListener = object : VehicleListener {
        override fun onVehicleClicked(vehicle: Vehicle) {
            zoomToSelectedMarker(vehicle)
        }

    }


    /**
     * Method to Handle Vehicle Item Click
     * We are Displaying Vehicle Data on UI and Also zooming in on map
     * Here we are collapsing our panel
     * Note in case of tablet device there will be no bottom sheet
     */
    fun zoomToSelectedMarker(currentVehicle: Vehicle) {

        mVehiclesUIHandler.setCurrentFleetType(currentVehicle.fleetType)
        mVehiclesUIHandler.setId(currentVehicle.id)
        mVehiclesUIHandler.setInfoViewShow(true)
        if (!isTablet)
            sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

        mMap?.let {
            val cameraPosition = CameraPosition.Builder()
                .target(
                    LatLng(currentVehicle.latitude, currentVehicle.longitude)
                )
                .zoom(it.maxZoomLevel - 5).build();
            it.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 2500, null)
        }
    }


    /**
     * Method to Populate Markers on Map
     */
    fun setMapMarkers(mVehicleList: List<VehicleView>) {
        try {
            val builder = LatLngBounds.Builder()
            mVehicleList.map {
                builder.include(mapMapper.mapToView(it).position)
                mMap?.addMarker(mapMapper.mapToView(it))
            }
            val bounds = builder.build()
            mMap?.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 0))

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    /**
     * Map State Listeners The Methods will be used to get Map bound Query database and get markers within bounds
     **/
    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        mMap?.let {
            it.setOnCameraIdleListener(this)
            it.setOnCameraMoveStartedListener(this)
            it.setOnCameraMoveListener(this)
            it.setOnCameraMoveCanceledListener(this)
        }
    }


    override fun onCameraMoveStarted(reasion: Int) {
        when (reasion) {
            GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE -> {
                if (isIdle) {
                    isIdle = false
                }
            }

        }
    }

    override fun onCameraMove() {
    }

    override fun onCameraMoveCanceled() {
    }

    override fun onCameraIdle() {
        if (!isIdle) {
            isIdle = true
            getVehiclesInMapBounds()
        }
    }


    /**
     * Method to Get Vehicles in Map Bounds
     */
    fun getVehiclesInMapBounds() {
        mMap?.let {
            val mCurrentLatLngBounds = it.getProjection().getVisibleRegion().latLngBounds
            getVehicleViewModel.fetchVehcilesInMapBound(
                mCurrentLatLngBounds.southwest.latitude,
                mCurrentLatLngBounds.southwest.longitude,
                mCurrentLatLngBounds.northeast.latitude,
                mCurrentLatLngBounds.northeast.longitude
            )
        }
    }

    /**
     * Method to Cancel Info View
     */
    override fun onCancelClick() {
        mVehiclesUIHandler.setInfoViewShow(false)
    }


}