package com.askvider.injection.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.askvider.FreeNowApplication
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * @author Ehtisham haq
 * The Module Provide all application level dependencies
 **/
@Module
abstract class ApplicationModule {


    /**
     * Provide the application context
     **/
    @Binds
    abstract fun bindContext(application: Application): Context


    @Binds
    abstract fun bindApplication(app: FreeNowApplication): Application


    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideSharedPreferences(
            app: Application
        ): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)
    }


}
