package com.askvider.injection.module

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.askvider.activities.main.fragments.HomeFragment
import com.askvider.injection.FragmentKey
import com.askvider.injection.InjectingFragmentFactory
import com.askvider.activities.vehicle.fragments.MapFragment
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class FragmentBindingModule{


    @Binds
    @IntoMap
    @FragmentKey(MapFragment::class)
    abstract fun bindMainFragment(mainFragment: MapFragment): Fragment

    @Binds
    abstract fun bindFragmentFactory(factory: InjectingFragmentFactory): FragmentFactory

    @Binds
    @IntoMap
    @FragmentKey(HomeFragment::class)
    abstract fun bindHomeFragment(homeFragment: HomeFragment):Fragment

}