package com.askvider.injection.module

import android.app.Application
import com.cache.VehicleCacheImpl
import com.cache.db.VehiclesDatabase
import com.data.repository.VehicleCache
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * @author Ehtisham haq
 * The Module Provide all Cache Dependencies
 * In Kotlin, we declare functions inside companion object
 * of the class to make them static and annotate the function with @JvmStatic for instance-less access.
 **/
@Module
abstract class CacheModule {

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun providesDataBase(application: Application): VehiclesDatabase {
            return VehiclesDatabase.getInstance(application)
        }
    }


    @Binds
    abstract fun bindVehiclesCache(projectsCache: VehicleCacheImpl): VehicleCache
}