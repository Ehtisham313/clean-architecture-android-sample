package com.askvider.injection.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.askvider.injection.ViewModelFactory
import com.presentation.GetVehiclesViewModel
import com.presentation.SignUpViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

/**
 * @author Ehtisham haq
 * The Module Provide all Data Module Dependencies
 **/
@Module
abstract class PresentationModule {

    @Binds
    @IntoMap
    @ViewModelKey(GetVehiclesViewModel::class)
    abstract fun bindGetVehiclesViewModel(viewModel: GetVehiclesViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(SignUpViewModel::class)
    abstract fun bindSignUpViewModel(viewModel: SignUpViewModel):ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}

/**
 * Our Own Annotation which will be used to identify our view model class
 **/
@MustBeDocumented
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
annotation class ViewModelKey(val value: KClass<out ViewModel>)