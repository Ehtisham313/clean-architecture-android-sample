package com.askvider.injection.module


import com.data.repository.VehicleRemote
import com.presentation.BuildConfig
import com.remote.PoiVehicleListRemoteImpl
import com.remote.service.PoiNetworkService
import com.remote.service.PoiRetrofitServiceFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * @author Ehtisham haq
 * Module To Bind Remote Repositories Layer
 * We have a companion object to generate it static Object
 * The @JvmStatic Annotation make sure no Instance is generated
 **/
@Module
abstract class RemoteModule {


    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideGithubService(): PoiNetworkService {
            return PoiRetrofitServiceFactory.makePoiRetrofitService(BuildConfig.DEBUG)
        }
    }

    @Binds
    abstract fun bindProjectsRemote(projectsRemote: PoiVehicleListRemoteImpl): VehicleRemote
}