package com.askvider.injection.module

import com.data.VehiclesDataRepository
import com.domain.repository.POIRepository
import dagger.Binds
import dagger.Module

/**
 * @author Ehtisham haq
 * The Module Provide all Data Module Dependencies
 **/
@Module
abstract class DataModule {

    @Binds
    abstract fun bindDataRepository(dataRepository: VehiclesDataRepository): POIRepository
}