package com.askvider.injection.module

import com.domain.executor.PostExecutionThread
import com.askvider.UiThread
import com.askvider.activities.main.MainActivity
import com.askvider.activities.splash.SplashActivity
import com.askvider.activities.vehicle.NewVehicleActivity
import com.askvider.activities.vehicle.VehicleActivity
import com.askvider.activities.videoplayer.VideoPlayerActivity
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector


/**
 * @author Ehtisham haq
 * Module To Provide UIModule Dependencies
 * Since we are using AndroidSchedular.runOnUIThread() schedular
 * Which is pure Android Componnent. We are keeping it here in UI Module and providing it refernce to Domain layer
 **/
@Module
abstract class UiModule {

    @Binds
    abstract fun bindPostExecutionThread(uiThread: UiThread): PostExecutionThread

    @ContributesAndroidInjector(modules = [NavHostModule::class])
    abstract fun contributesNewVehicleActivity():NewVehicleActivity

    @ContributesAndroidInjector
    abstract fun contributesBrowseActivity(): VehicleActivity

    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [NavHostModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeVideoPlayerActivity():VideoPlayerActivity

}