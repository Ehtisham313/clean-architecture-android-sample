package com.askvider.injection

import com.askvider.FreeNowApplication
import com.askvider.injection.module.*
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


/**
* @author Ehtisham haq
 * Dagger Application Component Class, abstract class for which a fully-formed,
 * dependency-injected implementation is to be generated from a set of modules().
 **/
//@Singleton
//@Component(modules = arrayOf(
//    AndroidSupportInjectionModule::class,
//    ApplicationModule::class,
//    UiModule::class,
//    PresentationModule::class,
//    DataModule::class,
//    CacheModule::class,
//    RemoteModule::class))

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ApplicationModule::class,
        UiModule::class,
        PresentationModule::class,
        DataModule::class,
        CacheModule::class,
        RemoteModule::class
    ]
)
interface ApplicationComponent: AndroidInjector<FreeNowApplication> {

//    @Component.Builder
//    interface Builder {
//        @BindsInstance
//        fun application(application: Application): Builder
//        fun build(): ApplicationComponent
//    }

//    @Component.Factory
//    interface Factory {
//        fun create(@BindsInstance applicationContext: Context): ApplicationComponent
//    }

    @Component.Factory
    abstract class Factory : AndroidInjector.Factory<FreeNowApplication>


}