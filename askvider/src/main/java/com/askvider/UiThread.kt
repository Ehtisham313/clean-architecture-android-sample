package com.askvider

import com.domain.executor.PostExecutionThread
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

/**
 * @author Ehtisham haq
 * The Thread class provide Executor Thread, in our case it will be mainThread provided by rxAndroid Library
 **/
class UiThread @Inject constructor(): PostExecutionThread {

    override val scheduler: Scheduler
        get() = AndroidSchedulers.mainThread()
}