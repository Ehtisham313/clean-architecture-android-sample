package com.askvider

import com.askvider.injection.DaggerApplicationComponent
import dagger.android.*
import timber.log.Timber


/**
 * @author Ehtisham haq
 * The Application class will be used by our app
 * For Now it will be used to instiatie Daggers Component and Timber
 **/
class FreeNowApplication :DaggerApplication() {



    override fun applicationInjector(): AndroidInjector<out FreeNowApplication> =
        DaggerApplicationComponent.factory().create(this)



    override fun onCreate() {
        super.onCreate()
        setupTimber()
    }

    private fun setupTimber() {
        Timber.plant(Timber.DebugTree())
    }


}