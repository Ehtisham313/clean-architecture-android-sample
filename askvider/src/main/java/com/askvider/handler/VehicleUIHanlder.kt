package com.askvider.handler

import androidx.databinding.BaseObservable
import androidx.databinding.library.baseAdapters.BR
import javax.inject.Inject

/**
 * @author Ehtisham haq
 *  Obserable Event UI handler class to provide UI related Info
 *  We will use this class to Bind Values with Views and update Views State
 **/
class VehicleUIHandler @Inject constructor() : BaseObservable() {

    var fleetType: String = "Taxi"
    var id: String = ""
    var showInfoView = false
    var showLoadingView = false


    fun setLoadingViewVisibility(show: Boolean) {
        showLoadingView = show
        notifyPropertyChanged(BR._all)
    }

    fun setCurrentFleetType(fleetType: String) {
        this.fleetType = fleetType
        notifyPropertyChanged(BR._all)
    }

    fun setId(id: Int) {
        this.id = id.toString()
        notifyPropertyChanged(BR._all)
    }

    fun setInfoViewShow(show: Boolean) {
        showInfoView = show
        notifyPropertyChanged(BR._all)
    }

}