package com.askvider.handler

/**
 * Event handler Interface for all the click events
 **/
interface MainEventHandler{
    fun onProfileClick()
    fun onHomeClick()
    fun onChatClick()
    fun onNotificationClick()
}