package com.askvider.handler

/**
 * Event handler Interface for all the click events
 **/
interface VehicleEventHandler{
    fun onCancelClick()
}