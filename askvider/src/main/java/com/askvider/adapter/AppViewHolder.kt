package com.askvider.adapter

import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView


/**
 * @author Ehtisham haq
 * My Generic View Holder class to bind Adapters and Data object to Recycler Item Directly
 **/
class AppViewHolder(val viewDataBinding: ViewDataBinding) : RecyclerView.ViewHolder(viewDataBinding.root){

    /**
    * Method to Bind Object with Our Current View
    **/
    fun bind(obj: Any){
        viewDataBinding.setVariable(BR.obj,obj)
        viewDataBinding.executePendingBindings()
    }

    /**
     * Method to Bind Adapter with our current View
     **/
    fun bind(adapter: BaseAdapter) {
        viewDataBinding.setVariable(BR.adapter, adapter)
        viewDataBinding.executePendingBindings()
    }

}