package com.askvider.adapter.home

import com.askvider.R
import com.askvider.model.Vehicle
import javax.inject.Inject

class ChannelsAdapter @Inject constructor():com.askvider.adapter.BaseAdapter(){

    var vehiclesList: List<Vehicle> = arrayListOf()

    override fun getLayoutIdForPosition(position: Int): Int = R.layout.channels_item

    override fun getItemCount(): Int= vehiclesList.size

    override fun getObjForPosition(position: Int): Any= vehiclesList[position]


    public fun updateList(mVehicleList:List<Vehicle>){
        vehiclesList=mVehicleList
        notifyDataSetChanged()
    }

}