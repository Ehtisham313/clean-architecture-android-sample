package com.askvider.adapter.home

import android.net.Uri
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import androidx.databinding.BindingAdapter
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.askvider.R
import com.askvider.activities.vehicle.VehicleListener
import com.askvider.adapter.BaseAdapter
import com.askvider.model.Vehicle
import javax.inject.Inject


@BindingAdapter("app:vidUrl")
fun bindVideo(playerView: PlayerView, url: String) {

        lateinit var player: SimpleExoPlayer
        player = ExoPlayerFactory.newSimpleInstance(playerView.context)
        val dataSourceFactory = DefaultDataSourceFactory(
            playerView.context,
            Util.getUserAgent(playerView.context, "com.askvider")
        )

        val videoSource = ExtractorMediaSource.Factory(dataSourceFactory)
            .createMediaSource(Uri.parse(url))

        player.prepare(videoSource)
        player.playWhenReady = true
        playerView.hideController()
        playerView.setPlayer(player)
        playerView.hideController()
        player.volume = 0f
        player.repeatMode = Player.REPEAT_MODE_ALL
}

@BindingAdapter(
    "app:asyncText",
    "android:textSize",
    requireAll = false)
fun asyncText(view: TextView, text: CharSequence, textSize: Int?) {
    // first, set all measurement affecting properties of the text
    // (size, locale, typeface, direction, etc)
    if (textSize != null) {
        // interpret the text size as SP
        view.textSize = textSize.toFloat()
    }
    val params = TextViewCompat.getTextMetricsParams(view)
    (view as AppCompatTextView).setTextFuture(
        PrecomputedTextCompat.getTextFuture(text, params, null))
}
class RecentListingAdapter @Inject constructor(): BaseAdapter(){

    var vehiclesList: List<Vehicle> = arrayListOf()


    var recentListListener: RecentListingListener? = null

    override fun getLayoutIdForPosition(position: Int): Int = R.layout.recent_list_item

    override fun getItemCount(): Int= vehiclesList.size

    override fun getObjForPosition(position: Int): Any= vehiclesList[position]


    public fun updateList(mVehicleList:List<Vehicle>){
        vehiclesList=mVehicleList
        notifyDataSetChanged()
    }

    public fun onItemClicked(selectedVehicle: Vehicle){
        recentListListener?.onPictureClicked(selectedVehicle)
    }


    interface RecentListingListener {

        fun onPictureClicked(vehicleId: Vehicle)

    }


}


