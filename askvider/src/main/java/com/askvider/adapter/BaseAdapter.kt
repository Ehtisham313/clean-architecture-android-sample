package com.askvider.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

/**
 * @author Ehtisham haq
 * Base Adapter Class to Bind Recycler View Using Data Binding Utils
 **/
abstract class BaseAdapter: RecyclerView.Adapter<AppViewHolder>(){


    /**
     * Method to Bind Layout with View Holder
     **/
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppViewHolder {
        val layoutInflater= LayoutInflater.from(parent.context)
        val binding: ViewDataBinding = DataBindingUtil.inflate(layoutInflater,viewType,parent,false)
        val viewHolder=AppViewHolder(binding)
        viewHolder.bind(this)
        return viewHolder
    }


    /**
     * Method to get correct view for particular index
     **/
    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }


    /**
     * Method to pass data object to view
     * We will pass only data object to our Generic View Holder.
     * The Data binding lib will do the rest
     * @see MyViewHolder.bind
     **/
    override fun onBindViewHolder(holder: AppViewHolder, p1: Int) {
        val obj=getObjForPosition(p1)
        holder.bind(obj)
    }




    /**
     * Abstract Method to get the Object and Layout for Particular Position
     * These Methods will be implemented in Child Classes
     **/
    protected abstract fun getObjForPosition(position: Int): Any

    protected abstract fun getLayoutIdForPosition(position: Int): Int
}