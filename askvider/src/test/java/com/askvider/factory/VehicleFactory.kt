package com.askvider.factory

import com.presentation.model.VehicleView


object VehicleFactory {

    fun makeVehicleView(): VehicleView {
        return VehicleView(DataFactory.randomInt(),
                           DataFactory.randomDouble(),DataFactory.randomDouble(),
                           DataFactory.randomFleetType(),DataFactory.randomDouble()
                           )
    }

}