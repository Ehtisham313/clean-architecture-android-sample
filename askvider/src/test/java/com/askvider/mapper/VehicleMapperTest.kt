package com.askvider.mapper

import com.askvider.factory.VehicleFactory
import junit.framework.Assert.assertEquals
import org.junit.Test

class VehicleMapperTest {

    private val vehicleMapper = VehicleViewMapper()

    @Test
    fun mapToViewMapsData() {
        val vehicles = VehicleFactory.makeVehicleView()
        val vehiclesForUi = vehicleMapper.mapToView(vehicles)
        assertEquals(vehicles.id, vehiclesForUi.id)
        assertEquals(vehicles.latitude, vehiclesForUi.latitude)
        assertEquals(vehicles.longitude, vehiclesForUi.longitude)
        assertEquals(vehicles.fleetType, vehiclesForUi.fleetType)
        assertEquals(vehicles.heading, vehiclesForUi.heading)
    }

}