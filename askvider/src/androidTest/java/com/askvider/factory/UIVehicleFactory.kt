package com.askvider.factory

import com.domain.model.Vehicle
import com.presentation.model.VehicleView


object UIVehicleFactory {

    fun makeVehicleView(): VehicleView {
        return VehicleView(UIDataFactory.randomInt(),
                           UIDataFactory.randomDouble(),UIDataFactory.randomDouble(),
                           UIDataFactory.randomFleetType(),UIDataFactory.randomDouble()
                           )
    }

    fun makeVehicle(): Vehicle {
        return Vehicle(UIDataFactory.randomInt(),
            UIDataFactory.randomDouble(),UIDataFactory.randomDouble(),
            UIDataFactory.randomFleetType(),UIDataFactory.randomDouble()
        )
    }

}