package com.askvider.factory

import java.util.*
import java.util.concurrent.ThreadLocalRandom

object UIDataFactory {



    fun randomFleetType(): String {
        return if(randomBoolean()) "POOLING" else "TAXI"
    }

    fun randomDouble(): Double {
        return randomInt().toDouble()
    }

    fun randomUuid(): String {
        return UUID.randomUUID().toString()
    }

    fun randomInt(): Int {
        return ThreadLocalRandom.current().nextInt(0, 1000 + 1)
    }

    fun randomLong(): Long {
        return randomInt().toLong()
    }

    fun randomBoolean(): Boolean {
        return Math.random() < 0.5
    }

}