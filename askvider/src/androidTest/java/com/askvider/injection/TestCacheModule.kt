package com.askvider.injection

import android.app.Application
import com.cache.db.VehiclesDatabase
import com.data.repository.VehicleCache
import com.nhaarman.mockito_kotlin.mock
import dagger.Module
import dagger.Provides

@Module
object TestCacheModule {

    @Provides
    @JvmStatic
    fun provideDatabase(application: Application): VehiclesDatabase {
        return VehiclesDatabase.getInstance(application)
    }

    @Provides
    @JvmStatic
    fun provideVehiclesCache(): VehicleCache {
        return mock()
    }
}

