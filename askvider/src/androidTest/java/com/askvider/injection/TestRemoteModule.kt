package com.askvider.injection

import com.data.repository.VehicleCache
import com.nhaarman.mockito_kotlin.mock
import com.remote.service.PoiNetworkService
import dagger.Module
import dagger.Provides


@Module
object TestRemoteModule {

    @Provides
    @JvmStatic
    fun provideGithubService(): PoiNetworkService {
        return mock()
    }

    @Provides
    @JvmStatic
    fun provideVehiclesRemote(): VehicleCache {
        return mock()
    }

}