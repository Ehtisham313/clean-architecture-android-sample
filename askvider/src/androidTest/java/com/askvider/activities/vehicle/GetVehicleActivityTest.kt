package com.askvider.activities.vehicle

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.askvider.TestApplication
import com.domain.model.Vehicle
import com.askvider.R
import com.askvider.adapter.AppViewHolder
import com.askvider.factory.UIDataFactory
import com.askvider.factory.UIVehicleFactory
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class GetVehicleActivityTest {


    @Rule
    @JvmField
    val activity = ActivityTestRule<VehicleActivity>(VehicleActivity::class.java, false, false)

    @Test
    fun activityLaunches() {
        stubVehicleRepoGetVehicles(Flowable.just(listOf(UIVehicleFactory.makeVehicle())))
        stubVehicleRepoGetVehiclesInBound(Flowable.just(listOf(UIVehicleFactory.makeVehicle())))
        activity.launchActivity(null)
    }


    @Test
    fun vehicleDisplay() {
        val vehicles = listOf(UIVehicleFactory.makeVehicle(),
            UIVehicleFactory.makeVehicle(), UIVehicleFactory.makeVehicle())
        stubVehicleRepoGetVehicles(Flowable.just(vehicles))
        stubVehicleRepoGetVehiclesInBound(Flowable.just(vehicles))
        activity.launchActivity(null)

        vehicles.forEachIndexed { index, vehicles ->
            onView(withId(R.id.recycler_vehicles))
                .perform(RecyclerViewActions.scrollToPosition<AppViewHolder>(index))
           onView(withId(R.id.recycler_vehicles))
               .check(matches(hasDescendant(withText(vehicles.id.toString()))))
        }
    }

    private fun stubVehicleRepoGetVehicles(flowable: Flowable<List<Vehicle>>) {
        whenever(TestApplication.appComponent().vehiclesRepository().getVehicles())
            .thenReturn(flowable)
    }

    private fun stubVehicleRepoGetVehiclesInBound(flowable: Flowable<List<Vehicle>>) {
        whenever(TestApplication.appComponent().vehiclesRepository()
            .getVehiclesInBound(UIDataFactory.randomDouble(),UIDataFactory.randomDouble(),
                UIDataFactory.randomDouble(),UIDataFactory.randomDouble()))
            .thenReturn(flowable)
    }

}

