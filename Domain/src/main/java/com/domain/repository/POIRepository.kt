package com.domain.repository

import com.domain.model.Vehicle
import com.domain.model.signin.SignIn
import com.domain.model.signup.SignUp
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable

/**
 * @author Ehtisham haq
 * Interface will be used to perform all operations we are going to perform in our app
 **/
interface POIRepository {
    fun getVehicles(): Flowable<List<Vehicle>>
    fun getSelectedVehicle(): Observable<Vehicle>
    fun selectVehicle(vehicleID:Int):Completable
    fun unSelectVehicle(vehicleID:Int):Completable
    fun getVehiclesInBound(lat1:Double,lng1:Double,lat2:Double,lng2:Double):Flowable<List<Vehicle>>

    fun signUP(
        password: String,
        phone: String,
        lastName: String,
        firstName: String,
        email: String,
        confirmPassword: String):Flowable<SignUp>

    fun signIn(email: String,
               password: String
    ):Flowable<SignIn>

}