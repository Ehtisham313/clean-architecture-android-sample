package com.domain.executor

import io.reactivex.Scheduler

/**
 * @author Ehtisham Haq
 * Our Android Application Observer will be subscirbed on Main Thread. We will use Android Schedular.MainThread() to observe ON
 * The Above Thread is provided by rxAndroid Lib which is part of Android Framework.
 * Connecting directly to it will result in Reference in Domain layer
 * Interface will provide Abstraction to Domain Layer for RxJava Observer.
 **/
interface PostExecutionThread{
    val scheduler:Scheduler
}