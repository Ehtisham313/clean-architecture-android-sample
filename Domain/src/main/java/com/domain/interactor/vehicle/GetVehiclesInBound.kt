package com.domain.interactor.vehicle

import com.domain.executor.PostExecutionThread
import com.domain.interactor.BaseFlowable
import com.domain.interactor.BaseObservable
import com.domain.model.Vehicle
import com.domain.repository.POIRepository
import io.reactivex.Flowable
import io.reactivex.Observable
import javax.inject.Inject

/**
 * @author Ehtisham haq
 * Method to get Vehicles
 **/

open class GetVehiclesInBound @Inject constructor(
    private val poiRepository: POIRepository,
    postExecutionThread: PostExecutionThread
)
    : BaseFlowable<List<Vehicle>, GetVehiclesInBound.Params>(postExecutionThread) {
    public override fun buildUseCaseObservable(params: Params?): Flowable<List<Vehicle>> {
        if (params == null) throw IllegalArgumentException("Params can't be null!")
        return poiRepository.getVehiclesInBound(params.lat1,params.lat2,params.lat2,params.lng2)
    }

    data class Params constructor(val lat1: Double,val lng1:Double,val lat2:Double,val lng2:Double) {
        companion object {
            fun forProject(lat1: Double,lng1:Double,lat2:Double,lng2:Double): Params {
                return Params(lat1,lng1,lat2,lng2)
            }
        }
    }

}