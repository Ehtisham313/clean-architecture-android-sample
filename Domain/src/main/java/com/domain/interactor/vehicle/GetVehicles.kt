package com.domain.interactor.vehicle

import com.domain.executor.PostExecutionThread
import com.domain.interactor.BaseFlowable
import com.domain.interactor.BaseObservable
import com.domain.model.Vehicle
import com.domain.repository.POIRepository
import io.reactivex.Flowable
import io.reactivex.Observable
import javax.inject.Inject

/**
 * @author Ehtisham haq
 * Class to get Vehicles
 **/
open class GetVehicles @Inject constructor(
    private val poiRepository: POIRepository,
    postExecutionThread: PostExecutionThread
    )
    : BaseFlowable<List<Vehicle>, Nothing?>(postExecutionThread) {

    public override fun buildUseCaseObservable(params: Nothing?): Flowable<List<Vehicle>> {
        return poiRepository.getVehicles()
    }

}