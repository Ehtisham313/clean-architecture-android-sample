package com.domain.interactor.signup

import com.domain.executor.PostExecutionThread
import com.domain.interactor.BaseFlowable
import com.domain.model.signup.SignUp
import com.domain.repository.POIRepository
import io.reactivex.Flowable
import javax.inject.Inject

/**
 * @author Ehtisham haq
 * Method to get Vehicles
 **/

open class SignUp @Inject constructor(
    private val poiRepository: POIRepository,
    postExecutionThread: PostExecutionThread
)
    : BaseFlowable<SignUp, com.domain.interactor.signup.SignUp.Body>(postExecutionThread) {


    data class Body constructor(
        val password: String,
        val phone: String,
        val lastName: String,
        val firstName: String,
        val email: String,
        val confirmPassword: String) {
        companion object {
            fun forProject(
                password: String,
                phone: String,
                lastName: String,
                firstName: String,
                email: String,
                confirmPassword: String): Body {
                return Body(password,phone,lastName,firstName,email,confirmPassword)
            }
        }
    }

    override fun buildUseCaseObservable(params: Body?): Flowable<SignUp> {
        if (params == null) throw IllegalArgumentException("Params can't be null!")
        return poiRepository.signUP(params.password,params.phone,
            params.lastName,params.firstName,
            params.email,params.confirmPassword)
    }

}