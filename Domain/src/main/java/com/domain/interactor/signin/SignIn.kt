package com.domain.interactor.signin

import com.domain.executor.PostExecutionThread
import com.domain.interactor.BaseFlowable
import com.domain.interactor.signup.SignUp
import com.domain.model.signin.SignIn
import com.domain.repository.POIRepository
import io.reactivex.Flowable
import javax.inject.Inject

/**
 * @author Ehtisham haq
 * Method to get Vehicles
 **/

open class SignIn @Inject constructor(
    private val poiRepository: POIRepository,
    postExecutionThread: PostExecutionThread
    ) : BaseFlowable<SignIn, com.domain.interactor.signin.SignIn.Body>(postExecutionThread) {


    data class Body constructor(
        val email: String,
        val password: String
    ) {
        companion object {
            fun forProject(
                password: String,
                email: String
            ): Body {
                return Body(password = password, email = email)
            }
        }
    }

    override fun buildUseCaseObservable(params: Body?): Flowable<SignIn> {
        if (params == null) throw IllegalArgumentException("Params can't be null!")
        return poiRepository.signIn(
            password = params.password,
            email = params.email
        )
    }


}