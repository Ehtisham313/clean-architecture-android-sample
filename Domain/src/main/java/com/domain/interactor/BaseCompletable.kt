package com.domain.interactor

import com.domain.executor.PostExecutionThread
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.schedulers.Schedulers


/**
 * @author Ehtisham haq
 * The class is base Class for Completab;e
 * It have all the common code to avoid boilerplate for each use case.
 * @param in in some use case our Observer might Require permitives so we are passing params
 * @param postExecutionThread it provides abstraction to our Schudlar that our Observer is going to use.(In Our case it will be AndroidSchedular)
 * @see PostExecutionThread
 **/
abstract class BaseCompletable<in Params> constructor(
    private val postExecutionThread: PostExecutionThread
) {

    /**
     * It will keep the reference for all the observers, so we could easily dispose them all at once
     **/
    private val disposables = CompositeDisposable()

    /**
     * Method to Generate Obserable
     **/
    protected abstract fun buildUseCaseCompletable(params: Params? = null): Completable

    /**
     * Method to Execute the Completable
     * @param observr disposable Observable of Type T to observe
     * @param params nullable params to pass to our use case
     * Since we are expecting our data from network or cache we are subscrbing on IO(), for mathematical we will use bounded schedular Computation()
     * RxJava keep referencing To avoid memory link, our observer is added to composite disposable
     */
    open fun execute(observer: DisposableCompletableObserver, params: Params? = null) {
        val completable = this.buildUseCaseCompletable(params)
            .subscribeOn(Schedulers.io())
            .observeOn(postExecutionThread.scheduler)
        addDisposable(completable.subscribeWith(observer))
    }

    /**
     * Method to add dispoable observers to composite disposable
     * So they can be release at once
     */
    fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }


    /**
     * Method to dispose all disposable
     */
    fun dispose() {
        if (!disposables.isDisposed) disposables.dispose()
    }

}