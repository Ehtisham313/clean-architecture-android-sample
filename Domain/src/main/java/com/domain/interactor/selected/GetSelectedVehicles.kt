package com.domain.interactor.selected

import com.domain.executor.PostExecutionThread
import com.domain.interactor.BaseObservable
import com.domain.model.Vehicle
import com.domain.repository.POIRepository
import io.reactivex.Observable
import javax.inject.Inject

/**
 * @author Ehtisham haq
 * Method to get Vehicles
 **/

open class GetSelectedVehicles @Inject constructor(
    private val poiRepository: POIRepository,
    postExecutionThread: PostExecutionThread
)
    : BaseObservable<Vehicle, Nothing?>(postExecutionThread) {

    public override fun buildUseCaseObservable(params: Nothing?): Observable<Vehicle> {
        return poiRepository.getSelectedVehicle()
    }

}