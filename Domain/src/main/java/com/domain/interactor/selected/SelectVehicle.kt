package com.domain.interactor.selected

import com.domain.executor.PostExecutionThread
import com.domain.interactor.BaseCompletable
import com.domain.repository.POIRepository
import io.reactivex.Completable
import javax.inject.Inject

/**
 * @author Ehtisham haq
 * Class to Select Vehicle
 **/
open class SelectVehicle @Inject constructor(val poiRepository: POIRepository,
                                             val postExecutionThread: PostExecutionThread)
                         :BaseCompletable<SelectVehicle.Params>(postExecutionThread){

    public override fun buildUseCaseCompletable(params: Params?): Completable {
        if (params == null) throw IllegalArgumentException("Params can't be null!")
        return poiRepository.selectVehicle(params.projectId)
    }


    data class Params constructor(val projectId: Int) {
        companion object {
            fun forProject(projectId: Int): Params {
                return Params(projectId)
            }
        }
    }

}