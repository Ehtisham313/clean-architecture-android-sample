package com.domain.model


/**
 * @author Ehtisham Haq
 * Class contain the POI List Data Object For Domain layer
 **/
class Vehicle(val id:Int,
              val latitude:Double,val longitude:Double,
              val fleetType:String,val heading:Double)

