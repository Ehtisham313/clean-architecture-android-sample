package com.domain.model.signup

public data class SignUp(
    val result: Result? = null,
    val code: Int,
    val message: String,
    val status: Boolean
)
