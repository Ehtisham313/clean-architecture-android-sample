package com.domain.model.signup

data class Result(
    val token: String? = null
)
