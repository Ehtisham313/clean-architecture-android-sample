package com.domain.model.signin


data class SignIn(
    val result: Result,
    val code: Int,
    val message: String,
    val status: Boolean
)
