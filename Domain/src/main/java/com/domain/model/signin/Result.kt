package com.domain.model.signin

data class Result(
    val token: String? = null
)