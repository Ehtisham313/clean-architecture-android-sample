package com.domain.interactor.selected

import com.domain.executor.PostExecutionThread
import com.domain.factory.VehicleDataFactory
import com.domain.repository.POIRepository
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class SelectVehicleTest{

    private lateinit var mSelectVehicle: SelectVehicle
    @Mock lateinit var  poiRepository:POIRepository
    @Mock lateinit var executionThread: PostExecutionThread

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        mSelectVehicle= SelectVehicle(poiRepository,executionThread)
    }


    @Test
    fun selectVehicleCompletes() {
        stubSelectVehicleRepo(Completable.complete())
        val testObserver = mSelectVehicle.buildUseCaseCompletable(
            SelectVehicle.Params(VehicleDataFactory.randomInt())).test()
        testObserver.assertComplete()
    }

    @Test(expected = IllegalArgumentException::class)
    fun selectVehicleThrowsException() {
        mSelectVehicle.buildUseCaseCompletable().test()
    }

    @Test
    fun selectVehicleRepository() {
        val vehicleId = VehicleDataFactory.randomInt()
        stubSelectVehicleRepo(Completable.complete())
        mSelectVehicle.buildUseCaseCompletable(SelectVehicle.Params(vehicleId)).test()
        verify(poiRepository).selectVehicle(vehicleId)
    }

    private fun stubSelectVehicleRepo(completable: Completable) {
        whenever(poiRepository.selectVehicle(any()))
            .thenReturn(completable)
    }



}