package com.domain.interactor.selected

import com.domain.executor.PostExecutionThread
import com.domain.factory.VehicleDataFactory
import com.domain.interactor.vehicle.GetVehicles
import com.domain.model.Vehicle
import com.domain.repository.POIRepository
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class GetSelectedVehicleTest {

    private lateinit var mGetSelectedVehicles: GetSelectedVehicles

    @Mock
    lateinit var  poiRepository: POIRepository
    @Mock
    lateinit var executionThread: PostExecutionThread

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        mGetSelectedVehicles= GetSelectedVehicles(poiRepository,executionThread)
    }


    @Test
    fun getVehicleCompletes() {
        stubVehiclesRepositoryGetVehicle(
            Observable.just(VehicleDataFactory.makeVehicle()))

    }

    @Test
    fun getSelectedVehicleCallsRepository() {
        stubVehiclesRepositoryGetVehicle(
            Observable.just(VehicleDataFactory.makeVehicle()))
        mGetSelectedVehicles.buildUseCaseObservable().test()
        verify(poiRepository).getSelectedVehicle()
    }

    @Test
    fun getSelectedVehicleReturnsData() {
        val vehicle = VehicleDataFactory.makeVehicle()
        stubVehiclesRepositoryGetVehicle(Observable.just(vehicle))
        val testObserver = mGetSelectedVehicles.buildUseCaseObservable().test()
        testObserver.assertValue(vehicle)
    }

    private fun stubVehiclesRepositoryGetVehicle(observable: Observable<Vehicle>) {
        whenever(poiRepository.getSelectedVehicle())
            .thenReturn(observable)
    }





}