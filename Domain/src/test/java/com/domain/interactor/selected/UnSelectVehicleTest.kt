package com.domain.interactor.selected

import com.domain.executor.PostExecutionThread
import com.domain.factory.VehicleDataFactory
import com.domain.repository.POIRepository
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UnSelectVehicleTest{

    private lateinit var mUnSelectVehicle: UnSelectVehicle

    @Mock
    lateinit var  poiRepository: POIRepository

    @Mock
    lateinit var executionThread: PostExecutionThread

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        mUnSelectVehicle= UnSelectVehicle(poiRepository,executionThread)
    }


    @Test
    fun unSelectVehicleCompletes() {
        stubSelectVehicleRepo(Completable.complete())
        val testObserver = mUnSelectVehicle.buildUseCaseCompletable(
            UnSelectVehicle.Params(VehicleDataFactory.randomInt())).test()
        testObserver.assertComplete()
    }

    @Test(expected = IllegalArgumentException::class)
    fun unSelectVehicleThrowsException() {
        mUnSelectVehicle.buildUseCaseCompletable().test()
    }

    @Test
    fun unSelectVehicleRepository() {
        val vehicleId = VehicleDataFactory.randomInt()
        stubSelectVehicleRepo(Completable.complete())
        mUnSelectVehicle.buildUseCaseCompletable(UnSelectVehicle.Params(vehicleId)).test()
        verify(poiRepository).unSelectVehicle(vehicleId)
    }

    private fun stubSelectVehicleRepo(completable: Completable) {
        whenever(poiRepository.unSelectVehicle(any()))
            .thenReturn(completable)
    }



}