package com.domain.interactor.vehicle

import com.domain.executor.PostExecutionThread
import com.domain.factory.VehicleDataFactory
import com.domain.model.Vehicle
import com.domain.repository.POIRepository
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.sun.org.omg.CORBA.Repository
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetVehiclesInBoundTest{

    private lateinit var getVehiclesInBound: GetVehiclesInBound

    @Mock
    lateinit var mPoiRepository: POIRepository

    @Mock
    lateinit var postExecutionThread: PostExecutionThread

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        getVehiclesInBound= GetVehiclesInBound(mPoiRepository,postExecutionThread)
    }


    @Test
    fun getVehiclesInBoundCompletes(){
        stubVehicleRepositoryGetVehicleInBound(Flowable.just(VehicleDataFactory.makeVehicleList(2)))
        val testObserver=getVehiclesInBound.buildUseCaseObservable(GetVehiclesInBound.Params(any(),any(),any(),any())).test()
        testObserver.assertComplete()
    }

    @Test
    fun getVehiclesInBoundReturnData(){
        val vehicleList=VehicleDataFactory.makeVehicleList(2)
        stubVehicleRepositoryGetVehicleInBound(Flowable.just(vehicleList))
        val testObserver=getVehiclesInBound.buildUseCaseObservable(GetVehiclesInBound.Params(any(),any(),any(),any())).test()
        testObserver.assertValue(vehicleList)
    }


    @Test(expected = IllegalArgumentException::class)
    fun getVehicleInBoundThrowsExceptionIfParameterNull(){
        getVehiclesInBound.buildUseCaseObservable().test()
    }

    @Test
    fun getVehiclesInBoundCallRepository(){

        val vehicleList=VehicleDataFactory.makeVehicleList(2)
        stubVehicleRepositoryGetVehicleInBound(Flowable.just(vehicleList))
        getVehiclesInBound.buildUseCaseObservable(GetVehiclesInBound.Params(any(),any(),any(),any())).test()
        verify(mPoiRepository).getVehiclesInBound(any(),any(),any(),any())
    }


    private fun stubVehicleRepositoryGetVehicleInBound(flowable: Flowable<List<Vehicle>>){
        whenever(mPoiRepository.getVehiclesInBound(any(),any(),any(),any()))
            .thenReturn(flowable)
    }

}