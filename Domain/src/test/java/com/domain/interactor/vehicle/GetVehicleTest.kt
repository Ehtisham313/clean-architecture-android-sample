package com.domain.interactor.vehicle

import com.domain.executor.PostExecutionThread
import com.domain.factory.VehicleDataFactory
import com.domain.model.Vehicle
import com.domain.repository.POIRepository
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class GetVehicleTest {

    private lateinit var getVehicle: GetVehicles
    @Mock
    lateinit var poiRepository: POIRepository
    @Mock
    lateinit var postExecutionThread: PostExecutionThread

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        getVehicle= GetVehicles(poiRepository,postExecutionThread)
    }

    @Test
    fun getVehiclesCompletes() {
        stubVehiclesRepositoryGetVehicles(
            Flowable.just(VehicleDataFactory.makeVehicleList(2)))
        val testObserver = getVehicle.buildUseCaseObservable().test()
        testObserver.assertComplete()
    }

    @Test
    fun getVehiclesCallsRepository() {
        stubVehiclesRepositoryGetVehicles(
            Flowable.just(VehicleDataFactory.makeVehicleList(2)))
        getVehicle.buildUseCaseObservable().test()
        verify(poiRepository).getVehicles()
    }

    @Test
    fun getVehicleReturnsData() {
        val vehicles = VehicleDataFactory.makeVehicleList(2)
        stubVehiclesRepositoryGetVehicles(Flowable.just(vehicles))
        val testObserver = getVehicle.buildUseCaseObservable().test()
        testObserver.assertValue(vehicles)
    }

    private fun stubVehiclesRepositoryGetVehicles(observable: Flowable<List<Vehicle>>) {
        whenever(poiRepository.getVehicles())
            .thenReturn(observable)
    }



}