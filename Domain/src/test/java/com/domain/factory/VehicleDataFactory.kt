package com.domain.factory

import com.domain.model.Vehicle
import java.util.*
import java.util.concurrent.ThreadLocalRandom

/**
 * @author Ehtisham haq
 * Class to generate the test data
 **/
object VehicleDataFactory {


    fun randomInt(): Int {
        return ThreadLocalRandom.current().nextInt(0, 1000 + 1)
    }

    fun randomFleetType(): String {
        return if(randomBoolean()) "POOLING" else "TAXI"
    }

    fun randomDouble(): Double {
        return randomInt().toDouble()
    }

    fun randomBoolean(): Boolean {
        return Math.random() < 0.5
    }

    fun makeVehicle(): Vehicle {
        return Vehicle(randomInt(), randomDouble(), randomDouble(), randomFleetType(), randomDouble())
    }

    fun makeVehicleList(count:Int):List<Vehicle>{
        var vehicleList= mutableListOf<Vehicle>()
        repeat(count){
            vehicleList.add(makeVehicle())
        }
        return vehicleList
    }


}
