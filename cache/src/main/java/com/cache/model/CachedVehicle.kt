package com.cache.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.cache.db.VehicleConstants

@Entity(tableName = VehicleConstants.TABLE_NAME)
data class CachedVehicle(
    @PrimaryKey
    @ColumnInfo(name = VehicleConstants.COLUMN_VEHICLE_ID)
    var id: Int,
    @ColumnInfo(name =VehicleConstants.COLUMN_LAT)
    var vehicleLats: Double,
    @ColumnInfo(name =VehicleConstants.COLUMN_LNG)
    var vehicleLngs: Double,
    var fleetType: String,
    var bearing:Double,
    @ColumnInfo(name = VehicleConstants.COLUMN_IS_SELECTED)
    var isSelected: Boolean
)

