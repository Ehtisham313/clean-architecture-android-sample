package com.cache.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.cache.db.ConfigConstants

/**
 * @author Ehtisham haq
 * Table to Store DB Configuration
 **/
@Entity(tableName = ConfigConstants.TABLE_NAME)
data class Config(
    @PrimaryKey(autoGenerate = true)
    var id: Int = -1,
    var lastCacheTime: Long)