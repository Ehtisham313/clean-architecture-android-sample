package com.cache

import com.cache.db.VehiclesDatabase
import com.cache.mapper.CachedVehicleMapper
import com.cache.model.Config
import com.data.model.VehicleEntity
import com.data.repository.VehicleCache
import com.domain.model.Vehicle
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

/**
 * @author Ehtisham haq
 * The class implement all the cache methods.
 **/

class VehicleCacheImpl @Inject constructor(
    private val projectsDatabase: VehiclesDatabase,
    private val mapper: CachedVehicleMapper
) : VehicleCache {
    override fun getVehiclesInBound(lat1: Double, lng1: Double, lat2: Double, lng2: Double): Flowable<List<VehicleEntity>> {
        return projectsDatabase.cachedProjectsDao().getVehiclesInBound(lat1, lng1, lat2, lng2)
            .map {
                it.map { mapper.mapFromCached(it) }
            }
    }

    override fun clearVehicles(): Completable {
        return Completable.defer {
            projectsDatabase.cachedProjectsDao().deleteVehicles()
            Completable.complete()
        }
    }

    override fun saveVehicles(vehicles: List<VehicleEntity>): Completable {
        return Completable.defer {
            projectsDatabase.cachedProjectsDao().insertVehicles(
                vehicles.map { mapper.mapToCached(it) })
            Completable.complete()
        }
    }

    override fun getVehicles(): Flowable<List<VehicleEntity>> {
        return projectsDatabase.cachedProjectsDao().getVehicles()
            .map {
                it.map { mapper.mapFromCached(it) }
            }
    }

    override fun getSelectedVehicle(): Flowable<VehicleEntity> {
        return projectsDatabase.cachedProjectsDao().getSelectedVehicle()
            .map {
                mapper.mapFromCached(it)
            }

    }

    override fun selectVehicle(vehicleId: Int): Completable {
        return Completable.defer {
            projectsDatabase.cachedProjectsDao().selectUnSelectVehicle(true, vehicleId)
            Completable.complete()
        }
    }

    override fun unSelectVehicle(vehicleId: Int): Completable {
        return Completable.defer {
            projectsDatabase.cachedProjectsDao().selectUnSelectVehicle(false, vehicleId)
            Completable.complete()
        }
    }

    override fun areVehiclesCached(): Single<Boolean> {
        return projectsDatabase.cachedProjectsDao().getVehicles().isEmpty
            .map {
                !it
            }
    }

    override fun isVehiclesCacheExpired(): Single<Boolean> {
        val currentTime = System.currentTimeMillis()
        val expirationTime = (60 * 10 * 1000).toLong()
        return projectsDatabase.configDao().getConfig().onErrorReturn { Config(lastCacheTime = 0) }
            .toSingle(Config(lastCacheTime = 0L))
            .map {
                currentTime - it.lastCacheTime > expirationTime
            }
    }

    override fun setLastCacheTime(lastCache: Long): Completable {
        return Completable.defer {
            projectsDatabase.configDao().insertConfig(Config(lastCacheTime = lastCache))
            Completable.complete()
        }
    }


}