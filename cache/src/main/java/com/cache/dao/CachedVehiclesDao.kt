package com.cache.dao


import androidx.room.*
import com.cache.db.VehicleConstants.DELETE_VEHICLES
import com.cache.db.VehicleConstants.QUERY_GET_MARKERS_IN_BOUND
import com.cache.db.VehicleConstants.QUERY_SELECTED_VEHICLES
import com.cache.db.VehicleConstants.QUERY_UPDATE_SELECTED_STATUS
import com.cache.db.VehicleConstants.QUERY_UPDATE_UNSELECT_ALL_VEHICLES
import com.cache.db.VehicleConstants.QUERY_VEHICLES
import com.cache.model.CachedVehicle
import com.domain.model.Vehicle
import io.reactivex.Flowable


/**
 * @author Ehtisham haq
 * CachedVehiclesDao Operation
 **/

@Dao
abstract class CachedVehiclesDao {

    /**
     * Query to get the Vehicle List
     **/
    @Query(QUERY_VEHICLES)
    @JvmSuppressWildcards
    abstract fun getVehicles(): Flowable<List<CachedVehicle>>


    /**
     * Query to get the Insert Vehicles list
     **/
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    abstract fun insertVehicles(vehicles: List<CachedVehicle>)


    /**
     * Query to delete vehicle list
     **/
    @Query(DELETE_VEHICLES)
    abstract fun deleteVehicles()


    /**
     * Query to get selected vehicle
     **/
    @Query(QUERY_SELECTED_VEHICLES)
    abstract fun getSelectedVehicle(): Flowable<CachedVehicle>


    /**
     * Query to get select un select vehicle
     **/
    @Query(QUERY_UPDATE_SELECTED_STATUS)
    abstract fun selectUnSelectVehicle(isSelected: Boolean,
                                   vehiclesId: Int)


    /**
     * Query to un select previously selected vehicle
     **/
    @Query(QUERY_UPDATE_UNSELECT_ALL_VEHICLES)
    abstract fun unSelectAllVehicles()


    /**
     * Method to Combine two Queries
     **/
    @Transaction
    open fun setSelectUnSelectVehicle(isSelected: Boolean,
                        vehiclesId: Int) {
        unSelectAllVehicles()
        selectUnSelectVehicle(isSelected,vehiclesId)
    }

    /*
     * Method to fetch vehicles from Local Cache within Bounds, we are not using it but we can use it if we want to extend the project
     */
    @Query(QUERY_GET_MARKERS_IN_BOUND)
    @JvmSuppressWildcards
    abstract fun getVehiclesInBound(lat1:Double,lng1:Double,lat2:Double,lng2:Double):Flowable<List<CachedVehicle>>

}