package com.cache.dao


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cache.db.ConfigConstants
import com.cache.model.Config
import io.reactivex.Flowable
import io.reactivex.Maybe

/**
 * @author Ehtisham haq
 * Database Access Class to perform operations on Configuration Database
 **/

@Dao
abstract class ConfigDao {

    @Query(ConfigConstants.QUERY_CONFIG)
    abstract fun getConfig(): Maybe<Config>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertConfig(config: Config)

}