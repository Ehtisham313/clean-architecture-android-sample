package com.cache.db

object VehicleConstants {

    const val TABLE_NAME = "vehicles"

    const val COLUMN_VEHICLE_ID = "vehicle_id"

    const val COLUMN_IS_SELECTED = "is_selected"

    const val COLUMN_LAT="lat"
    const val COLUMN_LNG="lng"

    const val QUERY_VEHICLES = "SELECT * FROM $TABLE_NAME"

    const val DELETE_VEHICLES = "DELETE FROM $TABLE_NAME"

    const val QUERY_SELECTED_VEHICLES = "SELECT * FROM $TABLE_NAME " +
            "WHERE $COLUMN_IS_SELECTED = 1"


    const val QUERY_GET_MARKERS_IN_BOUND ="SELECT * FROM $TABLE_NAME " +
            "WHERE ( :lat1 < :lat2 AND $COLUMN_LAT BETWEEN :lat1 AND :lat2) OR ( :lat2 < :lat1 AND $COLUMN_LAT BETWEEN :lat2 AND :lat1) " +
            "AND (:lng1 < :lng2 AND $COLUMN_LNG BETWEEN :lng1 AND :lng2) OR (:lng2 < :lng1 AND $COLUMN_LNG BETWEEN :lng2 AND :lng1)"

    const val QUERY_UPDATE_SELECTED_STATUS = "UPDATE $TABLE_NAME " +
            "SET $COLUMN_IS_SELECTED = :isSelected WHERE " +
            "$COLUMN_VEHICLE_ID = :vehiclesId"

    const val QUERY_UPDATE_UNSELECT_ALL_VEHICLES="UPDATE $TABLE_NAME " +
            "SET $COLUMN_IS_SELECTED = 0"



}