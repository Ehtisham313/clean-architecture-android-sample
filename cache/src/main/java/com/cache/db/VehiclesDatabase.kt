package com.cache.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.cache.dao.CachedVehiclesDao
import com.cache.dao.ConfigDao
import com.cache.model.CachedVehicle
import com.cache.model.Config
import javax.inject.Inject


/**
 * @author Ehtisham haq
 * The Class to create Room Database
 * We will increment the Version every time database is updated.
 **/

@Database(entities = arrayOf(CachedVehicle::class, Config::class), version = 1)
abstract class VehiclesDatabase @Inject constructor(): RoomDatabase() {
    abstract fun cachedProjectsDao(): CachedVehiclesDao
    abstract fun configDao(): ConfigDao
    companion object {

        private var INSTANCE: VehiclesDatabase? = null
        private val lock = Any()


        /**
         * IT will return the database instance
         * we are using synchronized to block concurrent execution by multiple threads of block at same time
         **/
        public fun getInstance(context: Context): VehiclesDatabase {
            if (INSTANCE == null) {
                synchronized(lock) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                            VehiclesDatabase::class.java, "vehicles.db")
                            .build()
                    }
                    return INSTANCE as VehiclesDatabase
                }
            }
            return INSTANCE as VehiclesDatabase
        }
    }

}