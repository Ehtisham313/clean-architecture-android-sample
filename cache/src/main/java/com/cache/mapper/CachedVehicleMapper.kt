package com.cache.mapper

import com.cache.model.CachedVehicle
import com.data.model.VehicleEntity
import javax.inject.Inject

/**
 * @author Ehtisham haq
 * The Class to Map Response
 **/
open class CachedVehicleMapper @Inject constructor(): CacheMapper<CachedVehicle, VehicleEntity> {
    override fun mapFromCached(type: CachedVehicle): VehicleEntity {
        return VehicleEntity(type.id,type.vehicleLats,type.vehicleLngs,type.fleetType,type.bearing)
    }

    override fun mapToCached(type: VehicleEntity): CachedVehicle {
        return CachedVehicle(type.id,type.latitude,type.longitude,type.fleetType,type.heading,false)
    }


}