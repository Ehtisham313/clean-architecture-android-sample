package com.cache.mapper

/**
 * @author Ehtisham haq
 * Cache Mapper Interface provide method to convert datatypes
 * @param C represent the Cache Model
 * @param E represent the Data Layer Model Object
 **/

interface CacheMapper<C, E> {

    fun mapFromCached(type: C): E

    fun mapToCached(type: E): C

}