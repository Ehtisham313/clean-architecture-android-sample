package com.cache.factory

import com.cache.model.CachedVehicle
import com.data.model.VehicleEntity

object VehicleDataFactory {

    fun makeCachedVehicles(): CachedVehicle {
        return CachedVehicle(DataFactory.randomInt(),
            DataFactory.randomDouble(),DataFactory.randomDouble(),
            DataFactory.randomFleetType(),DataFactory.randomDouble(),
            false)
    }

    fun makeCachedvehicleInBound():CachedVehicle{
        return CachedVehicle(DataFactory.randomInt(),
            11.11,11.11,
            DataFactory.randomFleetType(),11.11,
            false)

    }

    fun makeSelectedCachedVehicle(): CachedVehicle {
        return CachedVehicle(DataFactory.randomInt(),
            DataFactory.randomDouble(),DataFactory.randomDouble(),
            DataFactory.randomFleetType(),DataFactory.randomDouble(),
            true)
    }

    fun makeVehicleEntity(): VehicleEntity {
        return VehicleEntity(DataFactory.randomInt(),
            DataFactory.randomDouble(),DataFactory.randomDouble(),
            DataFactory.randomFleetType(),DataFactory.randomDouble())
    }


    fun makeVehicleInBoundEnitity():VehicleEntity{
        return VehicleEntity(DataFactory.randomInt(),
            11.11,11.11,
            DataFactory.randomFleetType(),11.11)

    }

}