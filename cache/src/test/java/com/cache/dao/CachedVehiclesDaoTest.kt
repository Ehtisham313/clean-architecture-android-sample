package com.cache.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import com.cache.db.VehiclesDatabase
import com.cache.factory.DataFactory
import com.cache.factory.VehicleDataFactory
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment


/**
 *  @author Ehtisham haq
 *  We are using RobolectricTest class to test here
 *  This Module contain the Android Framework Related test
 *  Here We are also using InstantTaskExecutorRule provide By Arch to get instant result from Room DB
 **/
@RunWith(RobolectricTestRunner::class)
class CachedVehiclesDaoTest {

    @Rule
    @JvmField var instantTaskExecutorRule=InstantTaskExecutorRule()

    private val database= Room.inMemoryDatabaseBuilder(
        RuntimeEnvironment.application.applicationContext,
        VehiclesDatabase::class.java
    ).allowMainThreadQueries()
        .build()

    @After
    fun closeDB(){
        database.close()
    }


    @Test
    fun getVehicleReturnData(){
        val vehicle=VehicleDataFactory.makeCachedVehicles()
        database.cachedProjectsDao().insertVehicles(listOf(vehicle))
        val testObserver=database.cachedProjectsDao().getVehicles().test()
        testObserver.assertValue(listOf(vehicle))

    }

    @Test
    fun deleteVehiclesClearData(){
        val vehicle=VehicleDataFactory.makeCachedVehicles()
        database.cachedProjectsDao().insertVehicles(listOf(vehicle))
        database.cachedProjectsDao().deleteVehicles()
        val testObserver=database.cachedProjectsDao().getVehicles().test()
        testObserver.assertValue(emptyList())
    }

    @Test
    fun getVehiclesInBound(){
        val vehicle=VehicleDataFactory.makeCachedvehicleInBound()
        database.cachedProjectsDao().insertVehicles(listOf(vehicle))
        val testObserver=database.cachedProjectsDao().getVehiclesInBound(
            10.10, 10.10,
            13.10, 13.10
        ).test()
        testObserver.assertValue(listOf(vehicle))
    }


    @Test
    fun getSelectedVehicleReturnsData() {
        val vehicles = VehicleDataFactory.makeCachedVehicles()
        val selectedVehicle = VehicleDataFactory.makeSelectedCachedVehicle()
        database.cachedProjectsDao().insertVehicles(listOf(vehicles, selectedVehicle))
        val testObserver = database.cachedProjectsDao().getSelectedVehicle().test()
        testObserver.assertValue(selectedVehicle)
    }


    @Test
    fun setSelectedVehiclesUpdateStatus() {
        val vehicles = VehicleDataFactory.makeCachedVehicles()
        database.cachedProjectsDao().insertVehicles(listOf(vehicles))
        database.cachedProjectsDao().setSelectUnSelectVehicle(true, vehicles.id)
        vehicles.isSelected = true
        val testObserver = database.cachedProjectsDao().getSelectedVehicle().test()
        testObserver.assertValue(vehicles)
    }


    @Test
    fun setUnSelectedVehiclesUpdateStatus() {
        val vehicles = VehicleDataFactory.makeSelectedCachedVehicle()
        database.cachedProjectsDao().insertVehicles(listOf(vehicles))
        database.cachedProjectsDao().setSelectUnSelectVehicle(false, vehicles.id)
        vehicles.isSelected = false
        val testObserver = database.cachedProjectsDao().getSelectedVehicle().test()
        testObserver.assertEmpty()
    }


    @Test
    fun unSelectAllVehiclesTest(){
        val vehicles=VehicleDataFactory.makeSelectedCachedVehicle()
        database.cachedProjectsDao().insertVehicles(listOf(vehicles))
        database.cachedProjectsDao().unSelectAllVehicles()
        val testObserver=database.cachedProjectsDao().getSelectedVehicle().test()
        testObserver.assertEmpty()

    }


}