package com.cache

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import com.cache.db.VehiclesDatabase
import com.cache.factory.VehicleDataFactory
import com.cache.mapper.CachedVehicleMapper
import com.cache.model.CachedVehicle
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment

/**
 *  @author Ehtisham haq
 *  We are using RobolectricTest class to test here
 *  This Module contain the Android Framework Related test
 *  Here We are also using InstantTaskExecutorRule provide By Arch to get instant result from Room DB
 **/
@RunWith(RobolectricTestRunner::class)
class VehiclesCacheImplTest {

    //Its from Arch Life test component to provide instant task execution
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val database = Room.inMemoryDatabaseBuilder(
        RuntimeEnvironment.application.applicationContext,
        VehiclesDatabase::class.java)
        .allowMainThreadQueries()
        .build()
    private val entityMapper = CachedVehicleMapper()
    private val cache = VehicleCacheImpl(database, entityMapper)

    @Test
    fun clearTablesCompletes() {
        val testObserver = cache.clearVehicles().test()
        testObserver.assertComplete()
    }

    @Test
    fun saveVehiclesCompletes() {
        val vehicles = listOf(VehicleDataFactory.makeVehicleEntity())
        val testObserver = cache.saveVehicles(vehicles).test()
        testObserver.assertComplete()
    }

    @Test
    fun getVehiclesReturnsData() {
        val vehicles = listOf(VehicleDataFactory.makeVehicleEntity(),VehicleDataFactory.makeVehicleInBoundEnitity())
        cache.saveVehicles(vehicles).test()
        val testObserver = cache.getVehicles().test()
        testObserver.assertValue(vehicles)
    }



    @Test
    fun getVehiclesDataInBoundReturnData(){
        val vehicles = listOf(VehicleDataFactory.makeVehicleInBoundEnitity(),VehicleDataFactory.makeVehicleInBoundEnitity())
        cache.saveVehicles(vehicles).test()
        val testObserver = cache.getVehiclesInBound(
            10.00, 10.00,
                 14.00,14.00
        ).test()
        testObserver.assertValue(vehicles)

    }


    @Test
    fun areVehicleCacheReturnsData() {
        val vehicles = listOf(VehicleDataFactory.makeVehicleEntity())
        cache.saveVehicles(vehicles).test()

        val testObserver = cache.areVehiclesCached().test()
        testObserver.assertValue(true)
    }

    @Test
    fun setLastCacheTimeCompletes() {
        val testObserver = cache.setLastCacheTime(1000L).test()
        testObserver.assertComplete()
    }

    @Test
    fun isVehiclessCacheExpiredReturnsNotExpired() {
        cache.setLastCacheTime(System.currentTimeMillis() - 1000).test()
        val testObserver = cache.isVehiclesCacheExpired().test()
        testObserver.assertValue(false)
    }

}