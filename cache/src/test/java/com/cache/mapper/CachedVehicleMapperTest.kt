package com.cache.mapper



import com.cache.factory.VehicleDataFactory
import com.cache.model.CachedVehicle
import com.data.model.VehicleEntity
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class CachedVehicleMapperTest {

    private val mapper = CachedVehicleMapper()

    @Test
    fun mapFromCachedMapsData() {
        val model = VehicleDataFactory.makeCachedVehicles()
        val entity = mapper.mapFromCached(model)

        assertEqualData(model, entity)
    }

    @Test
    fun mapToCachedMapsData() {
        val entity = VehicleDataFactory.makeVehicleEntity()
        val model = mapper.mapToCached(entity)

        assertEqualData(model, entity)
    }

    private fun assertEqualData(model: CachedVehicle,
                                entity: VehicleEntity
    ) {
        assertEquals(model.id, entity.id)
        assertEquals(model.vehicleLats, entity.latitude)
        assertEquals(model.vehicleLngs, entity.longitude)
        assertEquals(model.bearing, entity.heading)
        assertEquals(model.fleetType, entity.fleetType)
    }

}